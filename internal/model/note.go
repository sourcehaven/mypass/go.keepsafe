package model

import (
	"fmt"
	"gorm.io/gorm"
)

type Note struct {
	gorm.Model
	Name      string
	Note      string
	Favourite bool
	FolderID  uint
	Folder    *Folder `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
}

func (m *Note) SetName(v string) {
	m.Name = v
}

func (m *Note) SetNote(v string) {
	m.Note = v
}

func (m *Note) SetFavourite(v bool) {
	m.Favourite = v
}

// String method for the Note struct
func (m *Note) String() string {
	return fmt.Sprintf("Note (Name: %s, Note: %s, Favourite: %t)", m.Name, m.Note, m.Favourite)
}
