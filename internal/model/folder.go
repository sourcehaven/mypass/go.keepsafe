package model

import (
	"fmt"
	"gorm.io/gorm"
)

type Folder struct {
	gorm.Model
	Name string `gorm:"unique"`
}

func (m *Folder) SetName(v string) {
	m.Name = v
}

func (m *Folder) String() string {
	return fmt.Sprintf("Name: %s", m.Name)
}
