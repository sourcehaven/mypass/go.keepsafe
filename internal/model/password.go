package model

import (
	"fmt"
	"gorm.io/gorm"
)

type Password struct {
	gorm.Model
	Name      string
	Username  string
	Password  string
	Site      string
	Note      string
	Favourite bool
	FolderID  uint
	Folder    *Folder   `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
	ParentID  uint      `gormx:"parentkey"`
	Parent    *Password `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
	ChildID   uint      `gormx:"childkey"`
	Child     *Password `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE"`
}

func (m *Password) SetName(v string) {
	m.Name = v
}

func (m *Password) SetUsername(v string) {
	m.Username = v
}

func (m *Password) SetPassword(v string) {
	m.Password = v
}

func (m *Password) SetSite(v string) {
	m.Site = v
}

func (m *Password) SetNote(v string) {
	m.Note = v
}

func (m *Password) SetFavourite(v bool) {
	m.Favourite = v
}

func (m *Password) MaskPassword() string {
	return MaskPassword(m.Password)
}

// String method for the Password struct
func (m *Password) String() string {
	return fmt.Sprintf(
		"Password (Name: %s, Username: %s, Password: %s, Site: %s, Note: %s, Favourite: %t)",
		m.Name,
		m.Username,
		MaskPassword(m.Password),
		m.Site,
		m.Note,
		m.Favourite,
	)
}

func MaskPassword(_ string) string {
	return "*****" // return a constant length mask
}
