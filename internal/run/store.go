package run

import (
	"database/sql"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/data"
	"gorm.io/gorm"
	"sync"
)

// Local is initialized when first using this package.
// The variable is assigned in init function.
var Local Store

type store struct {
	mtx  *sync.RWMutex
	db   *gorm.DB
	user string
	pw   string
}

type Store interface {
	Pw() string
	User() string
	DB() *gorm.DB
	Unlock(user, password string) (err error)
	Lock() (err error)
}

func (s *store) Pw() string {
	return s.pw
}

func (s *store) User() string {
	return s.user
}

func (s *store) DB() *gorm.DB {
	return s.db
}

func (s *store) Unlock(user, password string) (err error) {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	s.db = data.Connect("file::memory:")
	s.user = user
	s.pw = password
	return
}

func (s *store) Lock() (err error) {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	var db *sql.DB
	if db, err = s.db.DB(); err != nil {
		return err
	}
	if err = db.Close(); err != nil {
		return
	}
	return
}
