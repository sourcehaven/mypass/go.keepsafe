package run

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
)

type Config interface {
	AppDir() fyne.URI
	AppFile(string) (fyne.URI, error)
	TokenSize() uint
}

type appConfig struct {
	appDir    fyne.URI
	tokenSize uint
}

var DefaultConfig Config

func (c *appConfig) AppDir() fyne.URI {
	if c.appDir == nil {
		c.appDir = fyne.CurrentApp().Storage().RootURI()
	}
	return c.appDir
}

func (c *appConfig) AppFile(subPath string) (fyne.URI, error) {
	dir := c.AppDir()
	return storage.Child(dir, subPath)
}

func (c *appConfig) TokenSize() uint {
	if c.tokenSize == 0 {
		c.tokenSize = 64
	}
	return c.tokenSize
}
