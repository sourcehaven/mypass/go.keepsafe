package run

import (
	"sync"
)

func init() {
	Local = &store{mtx: &sync.RWMutex{}}
	DefaultConfig = &appConfig{}
}
