package run

import "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/srv"

func startService(c srv.Config) {
	server := srv.NewService(c)
	server.Graceful().Start()
}

func Service(c ...srv.Config) chan struct{} {
	if len(c) == 0 {
		c = make([]srv.Config, 1)
	}
	cfg := c[0]
	serviceChan := make(chan struct{})
	go func() {
		startService(cfg)
		<-serviceChan
	}()
	return serviceChan
}
