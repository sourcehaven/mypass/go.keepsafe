package run

import (
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/data"
	"path"
)

const (
	lockfile = ".~mypass.lock#"
)

// Cache should be used carefully. Only accessible after lazy initialization
// with the call to DefaultCache() function.
var Cache data.Cache

// DefaultCache returns the globally usable, lazily initialized, cache handler.
func DefaultCache() data.Cache {
	if Cache == nil {
		Cache = data.NewCache(data.CacheConfig{
			Lock:      data.NewFLock(path.Join(DefaultConfig.AppDir().Path(), lockfile)),
			IgnoreBak: false,
		})
	}
	return Cache
}
