package storage

import (
	"fyne.io/fyne/v2/storage"
	"strings"
)

// MakeDirs is an extension on fyne storage utils,
// as it will create all subdirectories as needed,
// when creating a directory.
func MakeDirs(p string) (err error) {
	segments := strings.Split(p, "/")
	if len(segments) == 0 {
		return
	}
	if segments[0] == "" {
		segments[0] = "/"
	}
	uri := storage.NewFileURI("")
	for _, segment := range segments {
		var exists bool
		if uri, err = storage.Child(uri, segment); err != nil {
			return
		}
		if exists, err = storage.Exists(uri); err != nil {
			return
		}
		if exists {
			continue
		}
		if err = storage.CreateListable(uri); err != nil {
			return
		}
	}
	return
}
