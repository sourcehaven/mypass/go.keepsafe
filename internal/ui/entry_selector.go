package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

func (a *App) newEntrySelector(
	passwordAction func(),
	noteAction func(),
	addressAction func(),
	creditCardAction func(),
	bankAccountAction func(),
) fyne.CanvasObject {
	createCard := func(title string, icon fyne.Resource, onTapped func()) fyne.CanvasObject {
		btn := widget.NewButtonWithIcon(title, icon, onTapped)
		vbox := container.NewBorder(nil, nil, nil, nil, btn)
		return vbox
	}

	grid := container.NewAdaptiveGrid(
		2,
		createCard("Password", theme.AccountIcon(), passwordAction),
		createCard("Note", theme.AccountIcon(), noteAction),
		createCard("Address", theme.AccountIcon(), addressAction),
		createCard("Credit Card", theme.AccountIcon(), creditCardAction),
		createCard("Bank Account", theme.AccountIcon(), bankAccountAction),
	)

	return grid
}
