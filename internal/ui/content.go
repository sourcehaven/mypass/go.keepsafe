package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/security"
	"math/rand"
	"strings"
)

func (a *App) newLogin(next fyne.CanvasObject) fyne.CanvasObject {
	title := widget.NewRichText(&widget.TextSegment{
		Style: widget.RichTextStyle{
			Alignment: fyne.TextAlignCenter,
			SizeName:  theme.SizeNameHeadingText,
			TextStyle: fyne.TextStyle{Bold: true},
		},
		Text: "Unlock",
	})
	username := widget.NewEntry()
	username.PlaceHolder = "Username"
	password := widget.NewPasswordEntry()
	password.PlaceHolder = "Password"
	return container.NewVBox(
		title,
		func() fyne.CanvasObject {
			c := &widget.Form{
				Items: []*widget.FormItem{
					{Text: "Username", Widget: username},
					{Text: "Password", Widget: password},
				},
				OnSubmit: func() {
					logrus.Debug("Username:", username.Text)
					logrus.Debug("Password:", model.MaskPassword(password.Text))

					token, err := a.GetUserToken(username.Text, password.Text)
					if err != nil {
						a.showCustomConfirmDialog("This user ", "COnfirm", "Dismiss", widget.NewLabel("Helo"),
							func(b bool) {

							})
					} else {
						a.session = NewUserSession(NewUserCredentials(username.Text, password.Text, token), a.config.sessionTimeOut)
						logrus.Debug("User session created.")

						a.Window.SetContent(next)
						a.Window.Resize(fyne.NewSize(480, 320))
					}

				},
				SubmitText: "Unlock",
			}
			return c
		}(),
	)

}

func navbar(w fyne.CanvasObject, folders chan []string) fyne.CanvasObject {
	var fids []widget.TreeNodeID
	var nav fyne.CanvasObject

	nav = &widget.Tree{
		ChildUIDs: func(id widget.TreeNodeID) []widget.TreeNodeID {
			switch id {
			case "":
				return []widget.TreeNodeID{"Categories", "Folders", "Tools"}
			case "Categories":
				return []widget.TreeNodeID{"All Items", "Passwords", "Notes", "Credit Cards", "Personal Info", "Trash"}
			case "Folders":
				// Folders are loaded dynamically from db
				return fids
			case "Tools":
				return []widget.TreeNodeID{"Password Generator"}
			}
			return []string{}
		},
		IsBranch: func(id widget.TreeNodeID) bool {
			return id == "" || id == "Categories" || id == "Folders" || id == "Tools"
		},
		CreateNode: func(branch bool) fyne.CanvasObject {
			return widget.NewLabel("err::unnamed")
		},
		UpdateNode: func(id widget.TreeNodeID, branch bool, o fyne.CanvasObject) {
			o.(*widget.Label).SetText(id)
		},
		OnSelected: func(id widget.TreeNodeID) {
			logrus.Debugf("err::action unset for %s", id)
			switch id {
			case "Folders":
				w = widget.NewLabel("This is the folder view")
			case "All Items":
				w = widget.NewLabel("This is the all items view")
			case "Passwords":
				w = widget.NewLabel("This is the password view")
			}
		},
	}

	// Listener on folder channel
	go func() {
		for {
			fnames := <-folders
			fids = make([]widget.TreeNodeID, len(fnames))
			for i, f := range fnames {
				fids[i] = f
			}
			logrus.Debug("refreshing folders item")
			nav.(*widget.Tree).RefreshItem("Folders")
		}
	}()

	return nav
}

func settingsContent() fyne.CanvasObject {
	return container.NewHBox(
		widget.NewLabel("This is the settings page"),
	)
}

func homeFolderResource() []string {
	n := rand.Intn(5) + 1
	phrase, err := security.GeneratePassphrase(uint(n))
	if err != nil {
		panic(err)
	}
	return strings.Split(phrase, " ")
}

func homeContent(folders chan []string) fyne.CanvasObject {
	return container.NewHBox(
		widget.NewLabel("Check this out!"),
		widget.NewButton("Rotate Folders", func() {
			folders <- homeFolderResource()
		}),
	)
}

func (a *App) newMainContent() fyne.CanvasObject {
	content := a.newEmptyContent(
		"Empty vault\nPress to create new entry",
		func() {
			entryWin := a.App.NewWindow("Create entry")
			entryWin.SetContent(a.newEntrySelector(
				func() {
					entryWin.SetTitle("Create new Password")
					entryWin.SetContent(a.newPasswordForm(&model.Password{}, func(m *model.Password) {
						entryWin.Close()
						a.StorePassword(m)
					}))
				},
				func() {
					entryWin.SetTitle("Create new Note")
					entryWin.SetContent(a.newNoteForm(&model.Note{}, func(m *model.Note) {
						entryWin.Close()
						a.StoreNote(m)
					}))
				},
				func() {
					entryWin.Close()
				},
				func() {
					entryWin.Close()
				},
				func() {
					entryWin.Close()
				},
			))
			entryWin.Show()
		},
	)
	return content
}
