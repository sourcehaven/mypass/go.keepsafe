package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func (a *App) newEmptyContent(btnTitle string, btnAction func(), icon ...fyne.Resource) fyne.CanvasObject {
	var ico fyne.Resource
	if len(icon) > 0 {
		ico = icon[0]
	}

	content :=
		widget.NewButtonWithIcon(
			btnTitle,
			ico,
			btnAction,
		)

	return container.NewStack(content)
}
