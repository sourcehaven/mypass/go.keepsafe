package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

// createLabeledEntry creates
func (a *App) createVerticalLabeledEntry(label string, entry fyne.CanvasObject) fyne.CanvasObject {
	return container.NewVBox(
		widget.NewCard("", "",
			container.NewVBox(
				widget.NewLabel(label),
				entry,
			),
		),
	)
}
