package ui

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestUserSession_Credentials(t *testing.T) {
	cred := &userCredentials{username: "test", password: "****"}
	s := NewUserSession(cred, time.Second)
	sessionCred, err := s.Credentials()
	assert.NoError(t, err)
	assert.Same(t, cred, sessionCred)
}

func TestUserSession_Invalidate(t *testing.T) {
	cred := &userCredentials{username: "test", password: "****"}
	s := NewUserSession(cred, time.Second)
	ok := s.Invalidate()
	sessionCred, err := s.Credentials()
	assert.Error(t, err)
	assert.Nil(t, sessionCred)
	assert.True(t, ok)

	// Calling invalidate second time to test if deadlock is avoided.
	ok = s.Invalidate()
	sessionCred, err = s.Credentials()
	assert.Error(t, err)
	assert.Nil(t, sessionCred)
	assert.False(t, ok)
}

func TestUserSession_Timeout(t *testing.T) {
	cred := &userCredentials{username: "test", password: "****"}
	s := NewUserSession(cred, time.Millisecond)
	time.Sleep(time.Millisecond * 2)
	sessionCred, err := s.Credentials()
	assert.Error(t, err)
	assert.Nil(t, sessionCred)
}

func TestUserSession_ResetTimer(t *testing.T) {
	cred := &userCredentials{username: "test", password: "****"}
	s := NewUserSession(cred, time.Millisecond*10)
	time.Sleep(time.Millisecond * 7)
	sessionCred, err := s.Credentials()
	assert.NoError(t, err)
	assert.Same(t, cred, sessionCred)
	// Should reset timer, and get valid credentials
	err = s.ResetTimer()
	assert.NoError(t, err)
	time.Sleep(time.Millisecond * 7)
	sessionCred, err = s.Credentials()
	assert.NoError(t, err)
	assert.Same(t, cred, sessionCred)
	// After sleep, session should time out now
	time.Sleep(time.Millisecond * 7)
	sessionCred, err = s.Credentials()
	assert.Error(t, err)
	assert.Nil(t, sessionCred)
}
