package ui

import "encoding/base64"

type UserCredentials interface {
	Username() string
	Password() string
	Token() string
	SetUsername(username string)
	SetPassword(password string)
}

type userCredentials struct {
	username string
	password string
	token    string
}

func NewUserCredentials(username string, password string, token []byte) UserCredentials {
	return &userCredentials{username: username, password: password, token: base64.StdEncoding.EncodeToString(token)}
}

func (u *userCredentials) Token() string {
	return u.token
}

func (u *userCredentials) Username() string {
	return u.username
}

func (u *userCredentials) Password() string {
	return u.password
}

func (u *userCredentials) SetUsername(username string) {
	u.username = username
}

func (u *userCredentials) SetPassword(password string) {
	u.password = password
}
