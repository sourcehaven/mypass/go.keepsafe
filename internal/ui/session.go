package ui

import (
	"errors"
	"github.com/sirupsen/logrus"
	"sync/atomic"
	"time"
)

type UserSession interface {
	Credentials() (UserCredentials, error) // Credentials retrieves the user's UserCredentials if the session is still active.
	ResetTimer() error                     // ResetTimer resets the timer to the original duration
	Invalidate() bool                      // Invalidate ends the session and cleans up resources, returns true if it was not already invalidated.
}

// userSession manages the user session with a timer.
type userSession struct {
	credentials UserCredentials // credentials holds the user's credentials required for the session.
	timer       *time.Timer     // timer is used to manage and reset the session timeout.
	timeout     time.Duration   // timeout specifies the duration for which the session is valid.
	expired     atomic.Bool     // expired is a flag for session validity - session can expire or be destroyed.
	invalidate  chan struct{}   // invalidate is a channel to signal invalidation of the user session
}

// NewUserSession creates a new session with a specified timeout duration.
func NewUserSession(cred UserCredentials, timeout time.Duration) UserSession {
	if timeout <= 0 {
		logrus.Error("timeout must be > 0")
		panic("timeout duration must be positive")
	}

	s := &userSession{
		credentials: cred,
		timer:       time.NewTimer(timeout),
		timeout:     timeout,
		expired:     atomic.Bool{},
		invalidate:  make(chan struct{}),
	}

	// Start a goroutine to monitor the timer
	go s.monitorTimer()

	return s
}

// monitorTimer waits for the timer to expire or for invalidation.
func (s *userSession) monitorTimer() {
	select {
	case <-s.timer.C:
	case <-s.invalidate:
	}
	s.expired.CompareAndSwap(false, true)
	logrus.Debug("user session has expired")
}

func (s *userSession) Credentials() (UserCredentials, error) {
	if s.expired.Load() {
		return nil, errors.New("session has expired or has been invalidated")
	}
	logrus.Debug("Retrieving credentials")
	return s.credentials, nil
}

func (s *userSession) Invalidate() (ok bool) {
	// Only invalidate if not already expired
	if s.expired.CompareAndSwap(false, true) {
		// Close the invalidate channel to signal that the session is invalidated
		// Closing the channel ensures that any goroutine waiting on this channel will be notified
		close(s.invalidate)
		if !s.timer.Stop() {
			<-s.timer.C
		}
		ok = true
	}
	return
}

func (s *userSession) ResetTimer() error {
	if s.expired.Load() {
		return errors.New("cannot reset timer, session has expired or has been invalidated")
	}

	// Timer needs to be drained before calling Reset
	if !s.timer.Stop() {
		<-s.timer.C
	}
	s.timer.Reset(s.timeout)
	logrus.Debug("session timer has been reset")
	return nil
}
