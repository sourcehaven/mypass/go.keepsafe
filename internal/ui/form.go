package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
)

func (a *App) newPasswordForm(m *model.Password, onSubmit func(m *model.Password)) fyne.CanvasObject {
	name := widget.NewEntry()
	name.Text = m.Name

	username := widget.NewEntry()
	username.Text = m.Username

	password := widget.NewPasswordEntry()
	password.Text = m.Password

	site := widget.NewEntry()
	site.Text = m.Site

	note := widget.NewMultiLineEntry()
	note.Text = m.Note

	favourite := widget.NewCheck("Check if favourite", func(b bool) {})
	favourite.SetChecked(m.Favourite)

	form := widget.NewForm(
		widget.NewFormItem("Name", name),
		widget.NewFormItem("Username", username),
		widget.NewFormItem("Password", password),
		widget.NewFormItem("Site", site),
		widget.NewFormItem("Note", note),
		widget.NewFormItem("Favourite", favourite),
	)
	form.OnSubmit = func() {
		m.SetName(name.Text)
		m.SetUsername(username.Text)
		m.SetPassword(password.Text)
		m.SetSite(site.Text)
		m.SetNote(note.Text)
		m.SetFavourite(favourite.Checked)

		logrus.Debug("Password form submitted")
		logrus.Debug(m.String())

		onSubmit(m)
	}

	return form
}

func (a *App) newNoteForm(m *model.Note, onSubmit func(m *model.Note)) fyne.CanvasObject {
	name := widget.NewEntry()
	name.Text = m.Name

	note := widget.NewMultiLineEntry()
	note.Text = m.Note

	favourite := widget.NewCheck("Is favourite?", func(checked bool) {})
	favourite.SetChecked(m.Favourite)

	form := widget.NewForm(
		widget.NewFormItem("Name", name),
		widget.NewFormItem("Note", note),
		widget.NewFormItem("Favourite", favourite),
	)
	form.OnSubmit = func() {
		m.SetName(name.Text)
		m.SetNote(note.Text)
		m.SetFavourite(favourite.Checked)

		logrus.Debug("Note form submitted")
		logrus.Debug(m.String())

		onSubmit(m)
	}

	return form
}
