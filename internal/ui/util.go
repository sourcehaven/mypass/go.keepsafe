package ui

import "fyne.io/fyne/v2"

func (a *App) newWindow(title string, content fyne.CanvasObject) fyne.Window {
	win := a.App.NewWindow(title)
	win.SetContent(content)
	win.Show()
	return win
}
