package ui

import (
	"fmt"
	"fyne.io/fyne/v2"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	serialv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/run"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/security"
)

func (a *App) GetUserToken(username string, password string) ([]byte, error) {
	var cache serialv0.Cache
	var userBin fyne.URI
	var err error

	if userBin, err = run.DefaultConfig.AppFile(fmt.Sprintf("%s.bin", username)); err != nil {
		logrus.WithError(err).Error("Failed to find user binary")
		return nil, err
	}
	if err = run.DefaultCache().Load(userBin, &cache); err != nil {
		logrus.WithError(err).Error("Failed to load cache")
		return nil, err
	}

	var token []byte
	if token, err = security.PlainBytesPw(cache.Password, password); err != nil {
		logrus.WithError(err).Error("Failed to get token")
		return nil, err
	}

	return token, nil
}

func (a *App) StorePassword(m *model.Password) {

}

func (a *App) StoreNote(m *model.Note) {

}
