package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/driver/desktop"
	"github.com/sirupsen/logrus"
	"strconv"
	"time"
)

type AppConfig struct {
	sessionTimeOut time.Duration
}

func NewAppConfig(sessionTimeout time.Duration) *AppConfig {
	return &AppConfig{sessionTimeout}
}

type App struct {
	fyne.App
	fyne.Window

	homeDir fyne.URI
	session UserSession
	config  *AppConfig
}

func NewApp(config *AppConfig) *App {
	a := app.New()

	func() {
		warnMissing := func(name, value string) {
			if value == "" {
				logrus.Warningf("Missing application metadata: %s", name)
			}
		}

		warnMissing("ID", a.Metadata().ID)
		if a.Metadata().Icon == nil {
			logrus.Warning("Missing application metadata: Icon")
		}
		warnMissing("Name", a.Metadata().Name)
		warnMissing("Build", strconv.Itoa(a.Metadata().Build))
		warnMissing("Version", a.Metadata().Version)
	}()

	a.Settings().SetTheme(newAppTheme())
	homeDir := a.Storage().RootURI()
	logrus.Infof("Home directory set to: %s", homeDir)

	w := a.NewWindow(a.Metadata().Name)
	w.SetMaster()

	a.SetIcon(a.Metadata().Icon)

	// Prepare systray in desktop environment
	if desk, ok := a.(desktop.App); ok {
		// Create menu
		m := fyne.NewMenu(a.Metadata().Name,
			fyne.NewMenuItem("Show", func() {
				w.Show()
			}))
		desk.SetSystemTrayMenu(m)
	}
	// TODO: closing the app on mobile should also hide the window
	//  instead of killing the app
	// Hide window instead of closing it
	w.SetCloseIntercept(func() {
		w.Hide()
	})

	return &App{a, w, homeDir, &userSession{}, config}
}

func (a *App) IsDesktop() bool {
	_, ok := a.App.(desktop.App)
	return ok
}

func (a *App) IsMobile() bool {
	return a.App.Driver().Device().IsMobile()
}

func (a *App) IsBrowser() bool {
	return a.App.Driver().Device().IsBrowser()
}

func (a *App) ShowAndRun() {
	a.SetContent(a.newAppScreen())
	a.Window.ShowAndRun()
}
