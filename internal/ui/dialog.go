package ui

import (
	"errors"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/dialog"
)

func (a *App) showErrorDialog(err string) {
	dialog.ShowError(errors.New(err), a.Window)
}

func (a *App) showConfirmDialog(title string, message string, callback func(bool)) {
	dlg := dialog.NewConfirm(title, message, callback, a.Window)
	dlg.Show()
}

func (a *App) showCustomConfirmDialog(title string, confirm string, dismiss string, content fyne.CanvasObject, callback func(bool)) {
	dlg := dialog.NewCustomConfirm(title, confirm, dismiss, content, callback, a.Window)
	dlg.Show()
}
