package ui

import "fyne.io/fyne/v2"

// test_isUserLoggedIn should be replaced with isUserLoggedIn later based on backend response
// TODO
func test_isUserLoggedIn() bool {
	return true
}

func (a *App) newLoginScreen() fyne.CanvasObject {
	// Define the main content
	main := a.newMainContent()
	// Define login screen
	login := a.newLogin(main)

	return login
}

func (a *App) newMainScreen() fyne.CanvasObject {
	// Define the main content
	main := a.newMainContent()

	return main
}

func (a *App) newAppScreen() fyne.CanvasObject {
	if test_isUserLoggedIn() {
		return a.newMainScreen()
	}
	return a.newLoginScreen()
}
