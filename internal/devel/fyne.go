package devel

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/assets"
)

func InitFyne() {
	// Setting metadata only seems to be necessary
	// during local development, as FyneApp.toml
	// most likely is not picked up by the application.
	meta := fyne.AppMetadata{
		ID:      "io.sourcehaven.mypass.keepsafe",
		Name:    "MyPass",
		Version: "0.1.0-devel",
	}
	meta.Icon = assets.LogoIcon()
	app.SetMetadata(meta)
}
