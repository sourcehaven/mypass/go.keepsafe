package main

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/data"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	dsv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/data/v0alpha"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/security"
	datapkg "gitlab.com/sourcehaven/mypass/go.lcdbman/pkg/data"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type mockClient struct {
	vault map[string][]byte
}

func (m *mockClient) Sync(ctx context.Context, in *dsv0.SyncRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	m.vault[in.Uid] = in.Data
	return &emptypb.Empty{}, nil
}

func (m *mockClient) Fetch(ctx context.Context, in *dsv0.FetchRequest, opts ...grpc.CallOption) (*dsv0.FetchResponse, error) {
	return &dsv0.FetchResponse{Data: m.vault[in.Uid]}, nil
}

func mock() dsv0.ByteVaultServiceClient {
	return &mockClient{vault: make(map[string][]byte)}
}

func main() {
	logrus.SetLevel(logrus.DebugLevel)
	// Create grpc client
	// To create the real client use
	// client := srv.NewInsecureVaultClient()
	client := mock()
	// Your desired test user id
	uid := "31"
	// Your dummy password
	password := "password"
	// Generate key from password
	salt := make([]byte, security.SaltSize)
	key, err := security.KeyGen(password, &salt)
	if err != nil {
		panic(err)
	}
	// DB and dao
	db := data.Connect("file::memory:")
	if err = db.AutoMigrate(
		&model.Password{},
		&model.Folder{},
		&model.Note{},
	); err != nil {
		panic(err)
	}
	pwDao := datapkg.NewSimpleDao[uint, model.Password](datapkg.Config{DB: db})
	fdDao := datapkg.NewSimpleDao[uint, model.Folder](datapkg.Config{DB: db})

	// Create some test records
	if err = pwDao.Create(&model.Password{Name: "mypass", Username: "user", Password: "some-random-pw"}); err != nil {
		panic(err)
	}
	if err = pwDao.Create(&model.Password{Name: "thispass", Username: "user2", Password: "dsadaZUA", Site: "https://this.site.com"}); err != nil {
		panic(err)
	}
	if err = fdDao.Create(&model.Folder{Name: "important"}); err != nil {
		panic(err)
	}

	// Create serialization tool
	serializer := data.NewVaultSerializer(data.NewVaultStore(db))
	// Dump bytes so we can encrypt them later
	bytes, err := serializer.Dump()
	if err != nil {
		panic(err)
	}

	// Make encrypted vault from previously dumped bytes
	secure, err := security.SecureBytes(bytes, security.Config{Key: key, Salt: make([]byte, security.SaltSize)})
	if err != nil {
		return
	}

	// Make a sync request
	vault := &dsv0.SyncRequest{Data: secure, Uid: uid}
	if _, err = client.Sync(context.Background(), vault); err != nil {
		logrus.Fatalf("failed to sync datastore with %v", err)
	}

	// Fetch datastore
	var dbVault *dsv0.FetchResponse
	dbVault, err = client.Fetch(context.Background(), &dsv0.FetchRequest{Uid: uid})
	if err != nil {
		logrus.Fatalf("failed to fetch datastore with %v", err)
	}

	// Decrypt fetched vault
	plain, err := security.PlainBytes(dbVault.Data, security.Config{Key: key})
	if err != nil {
		panic(err)
	}
	print(salt)
	// Load vault into local db with our serialization tool
	if err = serializer.Load(plain); err != nil {
		return
	}

	// Examine results
	var pw []model.Password
	if pw, err = pwDao.FindAll(); err != nil {
		panic(err)
	}

	logrus.WithFields(logrus.Fields{
		"datastore": plain,
	}).Debug("debugging datastore store fetch api")
	logrus.WithFields(logrus.Fields{
		"passwords": pw,
	}).Debug("debugging datastore store fetch api")
}
