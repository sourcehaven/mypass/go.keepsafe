package data

import (
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	"gitlab.com/sourcehaven/mypass/go.lcdbman/pkg/data"
	"gorm.io/gorm"
)

type daoStore struct {
	pw data.Dao[uint, model.Password]
	fd data.Dao[uint, model.Folder]
	nt data.Dao[uint, model.Note]
}

func NewVaultStore(db *gorm.DB) VaultStore {
	return &daoStore{
		pw: data.NewSimpleDao[uint, model.Password](data.Config{DB: db}),
		fd: data.NewSimpleDao[uint, model.Folder](data.Config{DB: db}),
		nt: data.NewSimpleDao[uint, model.Note](data.Config{DB: db}),
	}
}

func (s *daoStore) AddPasswords(passwords []*model.Password) (err error) {
	if err = s.pw.Me2().Prune(); err != nil {
		return
	}
	return s.pw.CreateAll(passwords)
}

func (s *daoStore) Passwords() (m []model.Password, err error) {
	return s.pw.Me2().FindAll()
}

func (s *daoStore) AddNotes(notes []*model.Note) (err error) {
	if err = s.nt.Me2().Prune(); err != nil {
		return
	}
	return s.nt.CreateAll(notes)
}

func (s *daoStore) Notes() (n []model.Note, err error) {
	return s.nt.Me2().FindAll()
}

func (s *daoStore) AddFolders(folders []*model.Folder) (err error) {
	if err = s.fd.Me2().Prune(); err != nil {
		return
	}
	return s.fd.CreateAll(folders)
}

func (s *daoStore) Folders() (m []model.Folder, err error) {
	return s.fd.Me2().FindAll()
}
