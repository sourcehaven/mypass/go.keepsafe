package data

import (
	"github.com/gofrs/flock"
	"sync"
	"sync/atomic"
)

type RWLocker interface {
	Lock() (err error)
	Unlock() (err error)
	RLock() (err error)
	RUnlock() (err error)
	TryLock() (ok bool, err error)
	TryRLock() (ok bool, err error)
	Close() (err error)
}

type myRWMutex struct {
	rl     atomic.Int32
	wl     atomic.Int32
	locker *sync.RWMutex
}

func (m *myRWMutex) Lock() (err error) {
	m.locker.Lock()
	m.wl.Add(1)
	return
}

func (m *myRWMutex) Unlock() (err error) {
	m.locker.Unlock()
	m.wl.Add(-1)
	return
}

func (m *myRWMutex) Close() (err error) {
	if m.wl.Load() > 0 {
		m.locker.Unlock()
	}
	if m.rl.Load() > 0 {
		m.locker.RUnlock()
	}
	return
}

func (m *myRWMutex) RLock() (err error) {
	m.locker.RLock()
	m.rl.Add(1)
	return
}

func (m *myRWMutex) RUnlock() (err error) {
	m.locker.RUnlock()
	m.rl.Add(-1)
	return
}

func (m *myRWMutex) TryLock() (ok bool, err error) {
	m.locker.Lock()
	m.wl.Add(1)
	return true, nil
}

func (m *myRWMutex) TryRLock() (ok bool, err error) {
	m.locker.RLock()
	m.rl.Add(1)
	return true, nil
}

func NewRWMutex() RWLocker {
	return &myRWMutex{locker: &sync.RWMutex{}}
}

type myFlock struct {
	locker *flock.Flock
}

func NewFLock(lockfile string) RWLocker {
	return &myFlock{
		locker: flock.New(lockfile),
	}
}

func (m *myFlock) Lock() (err error) {
	return m.locker.Lock()
}

func (m *myFlock) Unlock() (err error) {
	if err = m.locker.Unlock(); err != nil {
		return
	}
	return
}

func (m *myFlock) RLock() (err error) {
	return m.locker.RLock()
}

func (m *myFlock) RUnlock() (err error) {
	return m.locker.Unlock()
}

func (m *myFlock) TryLock() (ok bool, err error) {
	return m.locker.TryLock()
}

func (m *myFlock) TryRLock() (ok bool, err error) {
	return m.locker.TryRLock()
}

func (m *myFlock) Close() (err error) {
	if err = m.locker.Close(); err != nil {
		return
	}
	return
}
