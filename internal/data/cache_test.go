package data

import (
	"crypto/rand"
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/storage"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/devel"
	serialv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"os"
	"path"
	"syscall"
	"testing"
)

func setupCache() {
	devel.InitFyne()
	_ = app.New()
}

func teardownCache() {
	fyne.CurrentApp().Quit()
}

func TestBackupUri(t *testing.T) {
	setupCache()
	defer teardownCache()

	// Test for nice format
	tmp := os.TempDir()
	uri := storage.NewFileURI(path.Join(tmp, "bakuri.bin"))
	bak, err := backupUri(uri)
	assert.NoError(t, err)
	assert.Equal(t, bak.Path(), path.Join(tmp, "~.bakuri.bin.bak#"))

	// Test for bad format
	uri = bak
	bak, err = backupUri(uri)
	assert.ErrorIs(t, err, AlreadyBakErr)
	assert.Nil(t, bak)
}

func TestCache_Dump(t *testing.T) {
	setupCache()
	defer teardownCache()

	c := NewCache(CacheConfig{Lock: NewRWMutex(), IgnoreBak: true})
	tmp := os.TempDir()
	f := storage.NewFileURI(path.Join(tmp, "mypass-dummy.bin"))
	data := make([]byte, 128)
	var err error
	_, err = rand.Read(data)
	assert.NoError(t, err)
	m := serialv0.Cache{Data: data}
	assert.NoError(t, c.Dump(f, &m))
	data = make([]byte, 256)
	_, err = rand.Read(data)
	assert.NoError(t, err)
	m.Data = data
	assert.NoError(t, c.Dump(f, &m))
}

func TestCache_Load(t *testing.T) {
	setupCache()
	defer teardownCache()

	c := NewCache(CacheConfig{Lock: NewRWMutex(), IgnoreBak: true})
	f := storage.NewFileURI("/this/file/does/not/exist")
	m := serialv0.Cache{}
	err := c.Load(f, &m)
	assert.ErrorIs(t, err, syscall.ENOENT)
}

func TestCache_DumpWithBak(t *testing.T) {
	setupCache()
	defer teardownCache()

	c := NewCache(CacheConfig{Lock: NewRWMutex(), IgnoreBak: false})
	tmp := os.TempDir()
	f := storage.NewFileURI(path.Join(tmp, fmt.Sprintf(tempFmt, "mypass-dummy.bin")))
	writer, err := storage.Writer(f) // touch the bak file
	defer func() {
		assert.NoError(t, storage.Delete(f))
	}()
	assert.NoError(t, err)
	defer func() {
		assert.NoError(t, writer.Close())
	}()
	data := make([]byte, 128)
	_, err = rand.Read(data)
	assert.NoError(t, err)
	m := serialv0.Cache{Data: data}
	err = c.Dump(storage.NewFileURI(path.Join(tmp, "mypass-dummy.bin")), &m)
	assert.ErrorIs(t, err, BakFileErr)
}
