package data

import (
	"errors"
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
	serialv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"google.golang.org/protobuf/proto"
	"regexp"
	"strings"
)

const (
	tempFmt = "~.%s.bak#" // backup file format string
)

var (
	AlreadyBakErr  = errors.New("file format is already a backup format")
	BakFileErr     = errors.New("backup file still in use")
	MalformedErr   = errors.New("resource does not exist or malformed")
	MultiAccessErr = errors.New("another instance of the application is already using this resource")
)

type cache struct {
	lock      RWLocker
	ignoreBak bool
}

type Cache interface {
	// Dump saves the cache in a binary format
	// represented by the Cache proto message.
	Dump(uri fyne.URI, cache *serialv0.Cache) (err error)
	// Load fetches the cache from disk and loads
	// the results into the Cache proto message in
	// a user readable format.
	Load(uri fyne.URI, cache *serialv0.Cache) (err error)
	// Bak determines if there are leftover backup
	// files for a given uri.
	Bak(uri fyne.URI) (exists bool, err error)
	// LoadBak loads the backed up content into the
	// format given by the Cache proto message.
	LoadBak(uri fyne.URI, cache *serialv0.Cache) (err error)
	// Clear will clean any leftover backup files
	// associated with the given uri.
	Clear(uri fyne.URI) (ok bool, err error)
}

type CacheConfig struct {
	Lock      RWLocker // the locking mechanism to use
	IgnoreBak bool     // signals to ignore the existence of backup files during Load and Dump operations
}

func NewCache(c ...CacheConfig) Cache {
	cfg := &CacheConfig{
		Lock: NewRWMutex(),
	}
	if len(c) > 0 {
		cfg = &c[0]
	}

	return &cache{
		lock:      cfg.Lock,
		ignoreBak: cfg.IgnoreBak,
	}
}

// dumpData will write desired data in a synchronized manner.
func (c *cache) dumpData(uri fyne.URI, dump []byte) (err error) {
	var bak fyne.URI
	if bak, err = backupUri(uri); err != nil {
		return
	}

	var writer fyne.URIWriteCloser
	var ok bool
	if ok, err = c.lock.TryLock(); err != nil || !ok {
		if !ok {
			err = MultiAccessErr
		}
		return
	}
	defer func(locker RWLocker) {
		if err2 := locker.Close(); err2 != nil {
			err = err2
		}
	}(c.lock)

	// Open temporary backup file for writing
	if writer, err = storage.Writer(bak); err != nil {
		return
	}
	var closed bool
	defer func(writer fyne.URIWriteCloser) {
		if closed {
			return
		}
		if err2 := writer.Close(); err2 != nil {
			err = err2
		}
	}(writer)

	// Write credentials to temporary file
	if _, err = writer.Write(dump); err != nil {
		return
	}
	if err = writer.Close(); err != nil { // explicit closing -> we need to move the file
		return
	}
	closed = true // signal closed state for deferred function

	// TODO: is copy safe enough? Consider move? (move replaces the file without any questions)
	//  Should be concerned about OS failure?
	if err = storage.Copy(bak, uri); err != nil {
		return // if move fails, this will leave behind the backup file
	}

	// Delete the backup resource ->
	// this is only necessary if copy operation is used instead of move operation.
	if err = storage.Delete(bak); err != nil {
		return
	}

	return
}

func (c *cache) Dump(uri fyne.URI, cache *serialv0.Cache) (err error) {
	// Check backup file existence and handle it
	// as given in the receiver's config
	if err = c.checkBakFile(uri); err != nil {
		return
	}
	// Prepare credentials dump
	var cacheDump []byte
	if cacheDump, err = proto.Marshal(cache); err != nil {
		return
	}

	if err = c.dumpData(uri, cacheDump); err != nil {
		return
	}
	return
}

// fetchData will read out data in a synchronized manner.
func (c *cache) fetchData(uri fyne.URI) (dump []byte, err error) {
	// Load the resource from storage
	var res fyne.Resource
	var ok bool
	if ok, err = c.lock.TryRLock(); err != nil || !ok {
		if !ok {
			err = MultiAccessErr
		}
		return
	}
	defer func(locker RWLocker) {
		if err2 := locker.Close(); err2 != nil {
			err = err2
		}
	}(c.lock)

	if res, err = storage.LoadResourceFromURI(uri); err != nil {
		return
	}

	// Read out credentials dump
	dump = res.Content()
	return
}

func (c *cache) Load(uri fyne.URI, cache *serialv0.Cache) (err error) {
	// Check backup file existence and handle it
	// as given in the receiver's config
	if err = c.checkBakFile(uri); err != nil {
		return
	}
	// Load the resource from storage
	var cacheDump []byte
	if cacheDump, err = c.fetchData(uri); err != nil {
		return
	}
	if err = proto.Unmarshal(cacheDump, cache); err != nil {
		return
	}
	return
}

func (c *cache) Bak(uri fyne.URI) (exits bool, err error) {
	var bak fyne.URI
	if bak, err = backupUri(uri); err != nil {
		return
	}
	return storage.Exists(bak)
}

func (c *cache) LoadBak(uri fyne.URI, cache *serialv0.Cache) (err error) {
	var bak fyne.URI
	if bak, err = backupUri(uri); err != nil {
		return
	}
	return c.Load(bak, cache)
}

func (c *cache) Clear(uri fyne.URI) (ok bool, err error) {
	var bak fyne.URI
	if bak, err = backupUri(uri); err != nil {
		return
	}
	if err = storage.Delete(bak); err != nil {
		return
	}
	ok = true
	return
}

func (c *cache) checkBakFile(uri fyne.URI) (err error) {
	if !c.ignoreBak {
		var exists bool
		if exists, err = c.Bak(uri); err != nil {
			return
		}
		if exists {
			err = BakFileErr
			return
		}
	}
	return
}

func backupUri(uri fyne.URI) (bak fyne.URI, err error) {
	name := uri.Name()
	pattern := regexp.QuoteMeta(tempFmt)
	pattern = strings.Replace(pattern, "%s", ".*", 1)
	var matched bool
	if matched, err = regexp.MatchString(pattern, name); err != nil {
		return
	}
	if matched {
		err = AlreadyBakErr
		return
	}
	bakName := fmt.Sprintf(tempFmt, name)
	var parent fyne.URI
	if parent, err = storage.Parent(uri); err != nil {
		return
	}
	if bak, err = storage.Child(parent, bakName); err != nil {
		return
	}
	return
}
