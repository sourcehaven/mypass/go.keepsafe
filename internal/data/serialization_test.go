package data

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"google.golang.org/protobuf/proto"
	"testing"
)

type dumpRes struct {
	dbPw []model.Password
	dbFd []model.Folder
	dbNt []model.Note
}

func setupDump() *dumpRes {
	dbPw := make([]model.Password, 0)
	dbFd := make([]model.Folder, 0)
	dbNt := make([]model.Note, 0)

	testPws := []model.Password{
		{Name: "cool password", Username: "whale", Password: "skSJDwisW83+s", Site: "https://thissize.com", Note: "unused pw", Favourite: false},
		{Name: "fakebook", Username: "fake", Password: "sand=(+ds7DE7", Site: "https://fakebook.org/auth", Favourite: true},
		{Name: "slurp", Username: "gulp", Password: "ioad()+ds9a(+DS", Site: "https://slurp.com", Note: "This is a note on Slurp platform.", Favourite: true},
		{Name: "littering", Username: "trash", Password: "EasY999", Site: "https://littering/auth/login", Note: "", Favourite: false},
	}
	testFds := []model.Folder{
		{Name: "social"},
	}
	testNts := []model.Note{
		{Name: "My first note", Note: "This note is very important!"},
		{Name: "My second note", Note: "This note is even more important!"},
	}

	for _, pw := range testPws {
		dbPw = append(dbPw, pw)
	}
	for _, fd := range testFds {
		dbFd = append(dbFd, fd)
	}
	for _, nt := range testNts {
		dbNt = append(dbNt, nt)
	}

	return &dumpRes{dbPw, dbFd, dbNt}
}

func teardownDump(r *dumpRes) {
	r.dbPw = make([]model.Password, 0)
	r.dbFd = make([]model.Folder, 0)
	r.dbNt = make([]model.Note, 0)
}

// region Array vaultStore test implementation

type arrayDumpStore struct {
	passwords *[]model.Password
	folders   *[]model.Folder
	notes     *[]model.Note
}

func (s *arrayDumpStore) Passwords() (m []model.Password, err error) {
	return *s.passwords, nil
}

func (s *arrayDumpStore) AddPasswords(pws []*model.Password) (err error) {
	*s.passwords = make([]model.Password, 0)
	for _, pw := range pws {
		*s.passwords = append(*s.passwords, *pw)
	}
	return
}

func (s *arrayDumpStore) Folders() (m []model.Folder, err error) {
	return *s.folders, nil
}

func (s *arrayDumpStore) AddFolders(fds []*model.Folder) (err error) {
	*s.folders = make([]model.Folder, 0)
	for _, fd := range fds {
		*s.folders = append(*s.folders, *fd)
	}
	return
}

func (s *arrayDumpStore) Notes() ([]model.Note, error) {
	return *s.notes, nil
}

func (s *arrayDumpStore) AddNotes(nts []*model.Note) (err error) {
	*s.notes = make([]model.Note, 0)
	for _, nt := range nts {
		*s.notes = append(*s.notes, *nt)
	}
	return
}

// endregion Array vaultStore test implementation

func TestDbSeri_Dump(t *testing.T) {
	r := setupDump()
	defer teardownDump(r)

	s := NewVaultSerializer(&arrayDumpStore{
		passwords: &r.dbPw,
		folders:   &r.dbFd,
		notes:     &r.dbNt,
	})
	data, err := s.Dump()
	assert.NoError(t, err)

	m := &v0alpha.Dump{}
	assert.NoError(t, proto.Unmarshal(data, m))
	assert.Len(t, m.Passwords, len(r.dbPw))
	assert.Len(t, m.Folders, len(r.dbFd))
	assert.Len(t, m.Notes, len(r.dbNt))
}

func TestDbSeri_Load(t *testing.T) {
	r := setupDump()
	defer teardownDump(r)

	dump := v0alpha.Dump{
		Passwords: make([]*v0alpha.Password, 5),
		Folders:   make([]*v0alpha.Folder, 3),
		Notes:     make([]*v0alpha.Note, 2),
	}
	data, err := proto.Marshal(&dump)
	assert.NoError(t, err)

	pws := make([]model.Password, 0)
	fds := make([]model.Folder, 0)
	nts := make([]model.Note, 0)
	s := NewVaultSerializer(&arrayDumpStore{
		passwords: &pws,
		folders:   &fds,
		notes:     &nts,
	})
	assert.NoError(t, s.Load(data))
	assert.Len(t, pws, len(dump.Passwords))
	assert.Len(t, fds, len(dump.Folders))
	assert.Len(t, nts, len(dump.Notes))
}
