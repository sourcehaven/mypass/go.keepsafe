/**
Implementations here helps the user dump the managed objects,
and database entities used by this project. For example, you
can easily dump your database objects to a byte array, as a
serialized object of course, which handles versioning, and
which format is more appropriate for encryption, and decryption.
*/

package data

import (
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"google.golang.org/protobuf/proto"
	"strconv"
)

type Serializer interface {
	Dump() ([]byte, error)
	Load([]byte) error
}

// region Vault specific store implementation

type VaultStore interface {
	AddPasswords([]*model.Password) error
	Passwords() ([]model.Password, error)
	AddNotes([]*model.Note) error
	Notes() ([]model.Note, error)
	AddFolders([]*model.Folder) error
	Folders() ([]model.Folder, error)
}

type vaultStore struct {
	store VaultStore
}

func NewVaultSerializer(s VaultStore) Serializer {
	return &vaultStore{store: s}
}

func (s *vaultStore) Dump() (data []byte, err error) {
	// Convert passwords to proto messages
	passwords, err := s.store.Passwords()
	if err != nil {
		return nil, err
	}
	protoPasswords := make([]*v0alpha.Password, len(passwords))
	for i, v := range passwords {
		protoPasswords[i] = &v0alpha.Password{
			Name:      v.Name,
			Username:  v.Username,
			Password:  v.Password,
			Site:      v.Site,
			Note:      v.Note,
			Favourite: v.Favourite,
			ParentId:  strconv.FormatUint(uint64(v.ParentID), 10),
			ChildId:   strconv.FormatUint(uint64(v.ChildID), 10),
			Meta:      toMeta(v.Model),
		}
	}
	// Convert folders to proto messages
	folders, err := s.store.Folders()
	if err != nil {
		return nil, err
	}
	protoFolders := make([]*v0alpha.Folder, len(folders))
	for i, v := range folders {
		protoFolders[i] = &v0alpha.Folder{
			Name: v.Name,
			Meta: toMeta(v.Model),
		}
	}

	// Convert notes to proto messages
	notes, err := s.store.Notes()
	if err != nil {
		return nil, err
	}
	protoNotes := make([]*v0alpha.Note, len(notes))
	for i, v := range notes {
		protoNotes[i] = &v0alpha.Note{
			Name:      v.Name,
			Note:      v.Note,
			Favourite: v.Favourite,
			FolderId:  strconv.FormatUint(uint64(v.FolderID), 10),
			Meta:      toMeta(v.Model),
		}
	}

	// Create dump
	dump := &v0alpha.Dump{
		Passwords: protoPasswords,
		Folders:   protoFolders,
		Notes:     protoNotes,
	}
	return proto.Marshal(dump)
}

func (s *vaultStore) Load(data []byte) (err error) {
	// Deserialize bytes as proto message
	dump := &v0alpha.Dump{}
	if err = proto.Unmarshal(data, dump); err != nil {
		return
	}
	// Save passwords
	passwords := make([]*model.Password, len(dump.Passwords))
	for i, v := range dump.Passwords {
		var folderId uint64
		if folderId, err = strconv.ParseUint(v.FolderId, 10, 64); err != nil && v.FolderId != "" {
			return
		}
		passwords[i] = &model.Password{
			Name:      v.Name,
			Username:  v.Username,
			Password:  v.Password,
			Site:      v.Site,
			Note:      v.Note,
			Favourite: v.Favourite,
			FolderID:  uint(folderId),
			Model:     toModel(v.Meta),
		}
	}
	err = s.store.AddPasswords(passwords)
	if err != nil {
		return
	}
	// Save folders
	folders := make([]*model.Folder, len(dump.Folders))
	for i, v := range dump.Folders {
		folders[i] = &model.Folder{
			Name:  v.Name,
			Model: toModel(v.Meta),
		}
	}
	err = s.store.AddFolders(folders)
	if err != nil {
		return
	}

	// Save folders
	notes := make([]*model.Note, len(dump.Notes))
	for i, v := range dump.Notes {
		var folderId uint64
		if folderId, err = strconv.ParseUint(v.FolderId, 10, 64); err != nil && v.FolderId != "" {
			return
		}
		notes[i] = &model.Note{
			Name:      v.Name,
			Note:      v.Note,
			Favourite: v.Favourite,
			FolderID:  uint(folderId),
			Model:     toModel(v.Meta),
		}
	}
	err = s.store.AddNotes(notes)
	if err != nil {
		return
	}
	return
}

// endregion Vault specific store implementation
