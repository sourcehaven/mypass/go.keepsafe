package data

import (
	gormv1 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/gorm/v1"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"
)

func toMeta(m gorm.Model) *gormv1.Meta {
	return &gormv1.Meta{
		Id:        uint64(m.ID),
		CreatedAt: timestamppb.New(m.CreatedAt),
		UpdatedAt: timestamppb.New(m.UpdatedAt),
		DeletedAt: &gormv1.NullTime{
			Time:  timestamppb.New(m.DeletedAt.Time),
			Valid: m.DeletedAt.Valid,
		},
	}
}

func toModel(m *gormv1.Meta) (model gorm.Model) {
	if m == nil {
		return
	}
	model.ID = uint(m.Id)
	if m.CreatedAt != nil {
		model.CreatedAt = m.CreatedAt.AsTime()
	}
	if m.UpdatedAt != nil {
		model.UpdatedAt = m.UpdatedAt.AsTime()
	}
	if m.DeletedAt != nil {
		model.DeletedAt.Time = m.DeletedAt.Time.AsTime()
		model.DeletedAt.Valid = m.DeletedAt.Valid
	}
	return
}
