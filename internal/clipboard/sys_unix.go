//go:build linux

package clipboard

import (
	"errors"
	"io"
	"os"
	"os/exec"
	"strings"
)

type displayManager int

const (
	unknown displayManager = iota
	x11
	wayland
)

var (
	NoDisplayManagerErr           = errors.New("no display manager found")
	NotSupportedDisplayManagerErr = errors.New("not supported display manager")
	NoClipboardToolErr            = errors.New("no clipboard tool found")
)

var display displayManager

func detectEnvDisplay() (dm displayManager, err error) {
	xdg := os.Getenv("XDG_SESSION_TYPE")
	switch xdg {
	case "wayland":
		return wayland, nil
	case "x11":
		return x11, nil
	case "":
		return unknown, NoDisplayManagerErr
	}
	return unknown, NotSupportedDisplayManagerErr
}

func detectLoginctlDisplay() (dm displayManager, err error) {
	// https://stackoverflow.com/questions/45135228/effective-way-of-detecting-x11-vs-wayland-preferrably-with-cmake
	// Should be fetched with the following command:
	_ = "loginctl user-status $USER | " +
		"grep -E -m 1 'session-[0-9]+\\.scope' | " +
		"sed -E 's/^.*?session-([0-9]+)\\.scope.*$/\\1/'"
	sessionId := "2"
	command := "loginctl"

	args := []string{
		"show-session",
		sessionId,
		"-p",
		"Type",
		"--value",
	}

	var out []byte
	if out, err = exec.Command(command, args...).Output(); err != nil {
		return unknown, err
	}
	xdg := string(out)
	xdg = strings.TrimSpace(xdg)

	switch xdg {
	case "wayland":
		return wayland, nil
	case "x11":
		return x11, nil
	case "":
		return unknown, NoDisplayManagerErr
	}
	return unknown, NotSupportedDisplayManagerErr
}

type simpleClipboardTool struct {
	cmd  string
	args []string
}

func (c *simpleClipboardTool) Make(args ...string) (cmd *exec.Cmd, err error) {
	allArgs := append(c.args, args...)
	cmd = exec.Command(c.cmd, allArgs...)
	return
}

type pipeClipboardTool struct {
	cmd  string
	args []string
}

func (c *pipeClipboardTool) Make(args ...string) (cmd *exec.Cmd, err error) {
	cmd = exec.Command(c.cmd, c.args...)
	var pipe io.WriteCloser

	// No arguments -> simple paste command
	if len(args) == 0 {
		return
	}

	// Got arguments -> pipe this to standard input
	if pipe, err = cmd.StdinPipe(); err != nil {
		return nil, err
	}
	defer func(pipe io.WriteCloser) {
		if err2 := pipe.Close(); err2 != nil {
			err = err2
		}
	}(pipe)

	cp := args[0]
	if _, err = pipe.Write([]byte(cp)); err != nil {
		return nil, err
	}
	return
}

func init() {
	var err error
	if display, err = detectEnvDisplay(); err != nil {
		panic(err)
	}

	switch display {
	case wayland:
		copyCmd = &simpleClipboardTool{
			cmd:  "wl-copy",
			args: []string{},
		}
		pasteCmd = &simpleClipboardTool{
			cmd:  "wl-paste",
			args: []string{"--no-newline"},
		}
		return
	case x11:
		var xselAvailable bool
		var xclipAvailable bool
		if _, err = exec.LookPath("xsel"); err == nil {
			xselAvailable = true
		}
		if _, err = exec.LookPath("xclip"); err == nil {
			xclipAvailable = true
		}

		if xselAvailable { // defaulting to xsel
			copyCmd = &pipeClipboardTool{
				cmd:  "xsel",
				args: []string{"--clipboard", "--input"},
			}
			pasteCmd = &pipeClipboardTool{
				cmd:  "xsel",
				args: []string{"--clipboard", "--output"},
			}
		} else if xclipAvailable {
			copyCmd = &pipeClipboardTool{
				cmd:  "xclip",
				args: []string{"-selection", "clipboard", "-in"},
			}
			pasteCmd = &pipeClipboardTool{
				cmd:  "xclip",
				args: []string{"-selection", "clipboard", "-out"},
			}
		}

		if copyCmd == nil || pasteCmd == nil {
			panic(NoClipboardToolErr)
		}

		return
	default:
		panic(NotSupportedDisplayManagerErr)
	}
}

func (s *sys) Push(data string) (err error) {
	cpy := *s.copy
	var cmd *exec.Cmd
	if cmd, err = cpy.Make(data); err != nil {
		return
	}
	return cmd.Run()
}

func (s *sys) Get() (data string, err error) {
	get := *s.paste
	var cmd *exec.Cmd
	if cmd, err = get.Make(); err != nil {
		return
	}
	var bin []byte
	if bin, err = cmd.Output(); err != nil {
		return
	}
	data = string(bin)
	return
}
