package clipboard

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSys_Clipboard(t *testing.T) {
	assert.NoError(t, System().Push("sth inside clipboard"))
	data, err := System().Get()
	assert.NoError(t, err)
	assert.Equal(t, "sth inside clipboard", data)
}
