package clipboard

type sys struct {
	copy  *clipboardCmd
	paste *clipboardCmd
}

// Specific platform implementations will be responsible
// for initializing these variables.
var copyCmd clipboardCmd
var pasteCmd clipboardCmd
var systemClipboard *sys

func System() Clipboard {
	if systemClipboard == nil {
		systemClipboard = &sys{
			copy:  &copyCmd,  // will be set, when the clipboard package is initialized
			paste: &pasteCmd, // will be set on initialization too
		}
	}
	return systemClipboard
}
