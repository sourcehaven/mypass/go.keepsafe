package clipboard

import "os/exec"

type Clipboard interface {
	// Push item onto the clipboard.
	Push(data string) (err error)
	// Get nth element of the clipboard.
	Get() (data string, err error)
}

type clipboardCmd interface {
	Make(...string) (cmd *exec.Cmd, err error)
}
