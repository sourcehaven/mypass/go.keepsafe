package security

import (
	"crypto/rand"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestPlainBytes(t *testing.T) {
	key := make([]byte, 32)
	_, err := rand.Read(key)
	assert.NoError(t, err)
	b := make([]byte, 168)
	_, err = rand.Read(b)
	assert.NoError(t, err)
	secure, err := SecureBytes(b, Config{Key: key, Salt: make([]byte, SaltSize)})
	assert.NoError(t, err)
	plain, err := PlainBytes(secure, Config{Key: key})
	assert.NoError(t, err)
	assert.True(t, reflect.DeepEqual(b, plain))
}

func TestPlainBytesPw(t *testing.T) {
	pw := "hellothere"
	b := make([]byte, 32)
	_, err := rand.Read(b)
	assert.NoError(t, err)
	secure, err := SecureBytesPw(b, pw)
	assert.NoError(t, err)
	assert.NotEmpty(t, secure)
	plain, err := PlainBytesPw(secure, pw)
	assert.NoError(t, err)
	assert.Equal(t, b, plain)
}
