package security

import (
	"crypto/rand"
	"encoding/binary"
	"errors"
	"fmt"
	cha "golang.org/x/crypto/chacha20poly1305"
)

var (
	EmptyKeyError       = errors.New("empty encryption key")
	EmptySaltError      = errors.New("salt used for key generation cannot be nil")
	InvalidKeySizeError = errors.New("invalid key size requires 32 bytes")
	CipherTooShortError = errors.New("cipher text too short")
)

// Config contains necessary information about
// securing a byte slice with encryption.
type Config struct {
	// Contains the encryption key
	Key []byte
	// Contains the salt used to generate the encryption key.
	// This is necessary so that simultaneously using the app
	// on different devices does not cause problems when
	// synchronizing with the actual data service.
	Salt []byte
}

// SecureBytes takes the given bytes as an argument,
// and encrypts them using XChaCha20 algorithm.
// The signing key, and salt should be passed as a Config variable.
//
// Code example:
//
//	// Generate, or get secure key from somewhere
//	key := make([]byte, 32)
//	if _, err := cryptorand.Read(key); err != nil {
//	  panic(err)
//	}
//	// Generate salt
//	salt := make([]byte, 16)
//	if _, err := cryptorand.Read(salt); err != nil {
//	  panic(err)
//	}
//	// Dummy bytes to encrypt
//	b := make([]byte, 168)
//	if _, err := rand.Read(b); err != nil {
//	  panic(err)
//	}
//	// Your freshly encrypted bytes
//	secure, err := SecureBytes(b, Config{Key: key, Salt: salt})
func SecureBytes(b []byte, c Config) (secure []byte, err error) {
	if c.Key == nil {
		return nil, EmptyKeyError
	}
	if c.Salt == nil {
		return nil, EmptySaltError
	}
	if len(c.Key) != cha.KeySize {
		return nil, InvalidKeySizeError
	}

	aead, err := cha.NewX(c.Key)
	if err != nil {
		// Should only panic, if wrong key length is given,
		// which we checked before
		panic(fmt.Sprintf("%v", err))
	}

	// Select a random nonce, and leave capacity for the ciphertext
	nonce := make([]byte, aead.NonceSize(), aead.NonceSize()+len(b)+aead.Overhead())
	if _, err = rand.Read(nonce); err != nil {
		panic(err)
	}
	// Encrypt the message and append the ciphertext to the nonce
	seal := aead.Seal(nonce, nonce, b, nil)

	// Put salt configuration in the front
	saltLen := len(c.Salt)
	saltBytes := make([]byte, 2, 2+saltLen) // uint16 will be stored in the first 2 bytes
	binary.LittleEndian.PutUint16(saltBytes, uint16(saltLen))
	saltBytes = append(saltBytes, c.Salt...)

	secure = make([]byte, 2+saltLen+len(seal))

	// Concat results
	copy(secure[:2+saltLen], saltBytes)
	copy(secure[2+saltLen:], seal)
	return
}

// SecureBytesPw encrypts a byte message using your password as key.
// The encryption key will be generated using a KDF, and a random salt.
// For key derivation Argon2ID algorithm is used, and the encryption uses
// the XChaCha20 algorithm.
//
// Code example:
//
//	// This is your password. This!
//	pw := "password"
//	// This will contain your encrypted message
//	var secure []byte
//	// Dummy bytes to encrypt
//	b := make([]byte, 168)
//	// Your freshly encrypted bytes
//	if secure, err := SecureBytesPw(b, pw); err != nil {
//	  panic(err)
//	}
func SecureBytesPw(b []byte, pw string) (secure []byte, err error) {
	var salt []byte
	var key []byte
	key, err = KeyGen(pw, &salt)
	return SecureBytes(b, Config{Key: key, Salt: salt})
}

// PlainBytes takes the given bytes as an argument,
// and decrypts them using XChaCha20 algorithm.
// The signing key should be passed as a Config variable.
//
// Code example:
//
//	// Encryption
//	// Generate, or get secure key from somewhere
//	key := make([]byte, 32)
//	if _, err := cryptorand.Read(key); err != nil {
//	  panic(err)
//	}
//	// Dummy bytes to encrypt
//	b := make([]byte, 168)
//	if _, err := rand.Read(b); err != nil {
//	  panic(err)
//	}
//	// Your freshly encrypted bytes
//	secure, err := SecureBytes(b, Config{Key: key})
//	if err != nil {
//		panic(err)
//	}
//	// Decryption
//	// Note that we are using the same key for encryption and decryption
//	plain, err := PlainBytes(secure, Config{Key: key})
//	if err != nil || !reflect.DeepEqual(b, plain) {
//	  fmt.Println("Opening vault failed")
//	} else {
//	  fmt.Println("Open sesame")
//	}
func PlainBytes(b []byte, c Config) (plain []byte, err error) {
	_, b = SplitSalt(b)
	if c.Key == nil {
		return nil, EmptyKeyError
	}
	if len(c.Key) != cha.KeySize {
		return nil, InvalidKeySizeError
	}

	aead, err := cha.NewX(c.Key)
	if err != nil {
		panic(fmt.Sprintf("%v", err))
	}

	if len(b) < aead.NonceSize() {
		return nil, CipherTooShortError
	}

	// Split nonce and ciphertext
	nonce, ciphertext := b[:aead.NonceSize()], b[aead.NonceSize():]
	// Decrypt the message and check it wasn't tampered with
	if plain, err = aead.Open(nil, nonce, ciphertext, nil); err != nil {
		return
	}
	return
}

// SplitSalt returns the salt, and data parts of the given bytes.
func SplitSalt(b []byte) (salt []byte, data []byte) {
	// Get salt size information from first 2 bytes
	sizeBytes := b[:2]
	saltLen := binary.LittleEndian.Uint16(sizeBytes)
	// Extract the salt
	salt = b[2 : 2+saltLen]
	data = b[2+saltLen:]
	return
}

// PlainBytesPw decrypts the given bytes using your password.
// The given byte slice contains the salt that was used to
// generate your key from your password. This salt is extracted,
// and then exactly the same salt will be used to decrypt your message.
//
// Code example:
//
//	// Encryption
//	// This is your password. This!
//	pw := "password"
//	// Dummy bytes to encrypt
//	b := make([]byte, 168)
//	// Your freshly encrypted bytes
//	secure, err := SecureBytesPw(b, pw)
//	if err != nil {
//	  panic(err)
//	}
//	// Decryption
//	// Note that we are using the same key for encryption and decryption
//	plain, err := PlainBytesPw(secure, pw)
//	if err != nil || !reflect.DeepEqual(b, plain) {
//	  fmt.Println("Opening vault failed")
//	} else {
//	  fmt.Println("Open sesame")
//	}
func PlainBytesPw(b []byte, pw string) (plain []byte, err error) {
	salt, _ := SplitSalt(b)

	// Generate valid key from password and salt
	var key []byte
	if key, err = KeyGen(pw, &salt); err != nil {
		return
	}

	// Use the original method for decryption
	return PlainBytes(b, Config{Key: key})
}
