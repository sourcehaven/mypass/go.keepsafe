package security

import (
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"golang.org/x/crypto/argon2"
	"strconv"
	"strings"
)

var (
	UnsupportedAlgorithmError = errors.New("unsupported algorithm")
	HashMalformedError        = errors.New("malformed hash")
	BadValueError             = errors.New("invalid value")
)

const (
	Algorithm = "argon2id"
	Version   = argon2.Version
	SaltSize  = 16
	Time      = 3
	Memory    = 32 * 1024
	Threads   = 4
	KeyLength = 32
)

type HashConfig struct {
	Algorithm string
	Version   int
	SaltSize  uint32
	Time      uint32
	Memory    uint32
	Threads   uint8
	KeyLength uint32
}

var argon2idConfig = HashConfig{
	Algorithm: Algorithm,
	Version:   Version,
	SaltSize:  SaltSize,
	Time:      Time,
	Memory:    Memory,
	Threads:   Threads,
	KeyLength: KeyLength,
}

// KeyGen generates a cryptographically secure key from your password.
// Optionally, you could also predefine a salt to be used during key generation,
// and you can pass a HashConfig too, if necessary.
//
// Code example:
//
//	key, err := KeyGen("password", nil)
//	if err != nil {
//	  panic(err)
//	}
//
// Alternatively, if you would like to save the generated salt,
// you could implement the following:
//
//	 var salt []byte
//	 key, err := KeyGen("password", &salt)
//		if err != nil {
//		  panic(err)
//		}
func KeyGen(password string, salt *[]byte, c ...HashConfig) (key []byte, err error) {
	if len(c) == 0 {
		c = []HashConfig{argon2idConfig}
	}
	cfg := c[0]
	if cfg.Algorithm != Algorithm {
		return nil, UnsupportedAlgorithmError
	}

	pw := []byte(password)
	if salt == nil {
		salt = new([]byte)
	}
	if len(*salt) == 0 {
		*salt = make([]byte, cfg.SaltSize)
		if _, err = rand.Read(*salt); err != nil {
			return
		}
	}
	key = argon2.IDKey(pw, *salt, cfg.Time, cfg.Memory, cfg.Threads, cfg.KeyLength)
	return
}

// PasswordHash hashes your password in a cryptographically secure way.
// Optionally, you could also predefine a salt to be used during key generation,
// and you can pass a HashConfig too, if necessary.
//
// Code example:
//
//	hash, err := PasswordHash("password", nil)
//	if err != nil {
//	  panic(err)
//	}
//
// Alternatively, if you would like to save the generated password,
// you could implement the following:
//
//	 var salt []byte
//	 hash, err := PasswordHash("password", &salt)
//		if err != nil {
//		  panic(err)
//		}
func PasswordHash(password string, salt *[]byte, c ...HashConfig) (hash string, err error) {
	if len(c) == 0 {
		c = []HashConfig{argon2idConfig}
	}
	cfg := c[0]
	if cfg.Algorithm != Algorithm {
		return "", UnsupportedAlgorithmError
	}

	if salt == nil {
		salt = new([]byte)
	}
	var key []byte
	if key, err = KeyGen(password, salt, cfg); err != nil {
		return
	}

	hash = fmt.Sprintf(
		"$%s$v=%d$t=%d,m=%d,p=%d$%s$%s",
		cfg.Algorithm,
		cfg.Version,
		cfg.Time,
		cfg.Memory,
		cfg.Threads,
		base64.StdEncoding.EncodeToString(*salt),
		base64.StdEncoding.EncodeToString(key),
	)
	return
}

func extractVal[T int | uint8 | uint32](param string, s string, conv func(string) (T, error)) (val T, err error) {
	value, found := strings.CutPrefix(s, fmt.Sprintf("%s=", param))
	if !found {
		return val, BadValueError
	}
	if val, err = conv(value); err != nil {
		return val, err
	}
	return
}

func extractArgon2idParts(hash string) (
	version int,
	time uint32,
	memory uint32,
	threads uint8,
	salt []byte,
	key []byte,
	err error,
) {
	splitter := string(hash[0])
	parts := strings.Split(hash, splitter)
	if len(parts) != 6 {
		err = HashMalformedError
		return
	}
	if version, err = extractVal[int]("v", parts[2], strconv.Atoi); err != nil {
		return
	}
	params := strings.Split(parts[3], ",")
	if len(params) != 3 {
		err = HashMalformedError
		return
	}
	if time, err = extractVal[uint32]("t", params[0], func(s string) (uint32, error) {
		val, err := strconv.ParseUint(s, 10, 32)
		return uint32(val), err
	}); err != nil {
		return
	}
	if memory, err = extractVal[uint32]("m", params[1], func(s string) (uint32, error) {
		val, err := strconv.ParseUint(s, 10, 32)
		return uint32(val), err
	}); err != nil {
		return
	}
	if threads, err = extractVal[uint8]("p", params[2], func(s string) (uint8, error) {
		val, err := strconv.ParseUint(s, 10, 32)
		return uint8(val), err
	}); err != nil {
		return
	}
	if salt, err = base64.StdEncoding.DecodeString(parts[4]); err != nil {
		return
	}
	if key, err = base64.StdEncoding.DecodeString(parts[5]); err != nil {
		return
	}
	return
}

// VerifyHash verifies your password by calling PasswordHash on
// password, and then checking if your original hash is the
// same as the to-be-verified one.
//
// Code example:
//
//	 // Hash your password
//	 hash, err := PasswordHash("password", nil)
//	 if err != nil {
//		 panic(err)
//	 }
//	 // Verify it
//	 ok, err := VerifyHash(hash, "password")
//	 if err != nil {
//	   panic(err)
//	 }
//	 if ok {
//	   fmt.Println("passwords do match")
//	 } else {
//	   fmt.Println("passwords do not match")
//	 }
func VerifyHash(hash string, password string) (ok bool, err error) {
	version, time, memory, threads, salt, key, err := extractArgon2idParts(hash)
	if err != nil {
		return false, err
	}

	check, err := PasswordHash(password, &salt, HashConfig{
		Algorithm: Algorithm,
		Version:   version,
		SaltSize:  uint32(len(salt)),
		Time:      time,
		Memory:    memory,
		Threads:   threads,
		KeyLength: uint32(len(key)),
	})
	if err != nil {
		return false, err
	}
	return check == hash, nil
}
