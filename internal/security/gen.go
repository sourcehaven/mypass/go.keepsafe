package security

import (
	"crypto/rand"
	"errors"
	"github.com/sethvargo/go-diceware/diceware"
	"math/big"
	"slices"
	"strings"
	"unicode"
)

var (
	InvalidLengthError = errors.New("invalid length, must be between [6, 64]")
	InvalidConfigError = errors.New("invalid config, there is nothing to generate")
	IterationError     = errors.New("too many iterations, password generation failed")
)

const (
	// MaxIters is the maximum number of iterations,
	// that the password generator will try to re-generate
	// the password so that it complies with the given
	// rule set, specified by the options.
	MaxIters = 256
	// LowerLetters is the list of lowercase letters.
	LowerLetters = "abcdefghijklmnopqrstuvwxyz"
	// UpperLetters is the list of uppercase letters.
	UpperLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	// Digits is the list of permitted digits.
	Digits = "0123456789"
	// Symbols is the list of symbols.
	Symbols = "@!$%&*#^"
	// MinLen is the length minimum that this algorithm will accept.
	MinLen = 6
	// MaxLen is the length maximum that this algorithm will accept.
	MaxLen = 64
	// ModerateThreshold specifies the limit to consider password strength moderate.
	ModerateThreshold = 0.5
	// StrongThreshold specifies the limit when a password becomes strong.
	StrongThreshold = 0.85
)

type PasswordStrength uint

const (
	Weak PasswordStrength = iota
	Moderate
	Strong
)

type PasswordOpt struct {
	Length       uint
	LowerLetters bool
	UpperLetters bool
	Digits       bool
	Symbols      bool
	ExtraSymbols string
}

func hasLower(s string) bool {
	for _, r := range s {
		if unicode.IsLower(r) {
			return true
		}
	}
	return false
}

func hasUpper(s string) bool {
	for _, r := range s {
		if unicode.IsUpper(r) {
			return true
		}
	}
	return false
}

func hasDigits(s string) bool {
	for _, r := range s {
		if unicode.IsDigit(r) {
			return true
		}
	}
	return false
}

func hasSymbols(s string) bool {
	for _, r := range s {
		if unicode.IsSymbol(r) || unicode.IsPunct(r) {
			return true
		}
	}
	return false
}

// strengthMetric calculates a given password's strength.
// This metric is very simplistic. It returns a number between
// zero and one, with the following rule set:
//   - if it has lower case -> give 0.25 points
//   - if it has upper case -> give 0.25 points
//   - if it has digits     -> give 0.25 points
//   - if it has symbols    -> give 0.25 points
//   - multiple points by password length divided by 12
//   - ensure that the results stay between [0, 1] interval
func strengthMetric(pw string) float32 {
	m := float32(0.0)
	const L = 12 // reference length
	l := float32(len(pw)) / L
	if hasLower(pw) {
		m += 0.25
	}
	if hasUpper(pw) {
		m += 0.25
	}
	if hasDigits(pw) {
		m += 0.25
	}
	if hasSymbols(pw) {
		m += 0.25
	}
	m = m * l
	if m < 0 {
		m = 0
	}
	if m > 1 {
		m = 1
	}
	return m
}

// Strength returns if a given password is weak, moderate, or strong.
func Strength(pw string) PasswordStrength {
	m := strengthMetric(pw)
	if m < ModerateThreshold {
		return Weak
	} else if m < StrongThreshold {
		return Moderate
	}
	return Strong
}

func generateFromSelection(sel []string, length uint) (pw string, err error) {
	gen := make([]string, length)
	sl := int64(len(sel))
	for i := range gen {
		var idx *big.Int
		if idx, err = rand.Int(rand.Reader, big.NewInt(sl)); err != nil {
			return
		}
		gen[i] = sel[idx.Int64()]
	}
	pw = strings.Join(gen, "")
	return
}

// intersect finds if there is an intersection between two string slices.
// https://stackoverflow.com/questions/44956031/how-to-get-intersection-of-two-slice-in-golang
func intersect(s1, s2 []string) bool {
	hash := make(map[string]bool)
	for _, e := range s1 {
		hash[e] = true
	}
	for _, e := range s2 {
		// If elements present in the hashmap then append intersection list
		if hash[e] {
			return true
		}
	}
	return false
}

// GeneratePassword will generate a unique crypto safe password
// based on the given parameters. In rare cases, it is possible,
// although highly unlikely, that during generation an iteration
// error occurs, which is entirely random in nature.
func GeneratePassword(opt PasswordOpt) (pw string, err error) {
	if opt.Length < MinLen || opt.Length > MaxLen {
		err = InvalidLengthError
		return
	}
	if opt.LowerLetters == opt.UpperLetters == opt.Digits == opt.Symbols == false && opt.ExtraSymbols == "" {
		err = InvalidConfigError
		return
	}
	// Create the selection of letters, symbols, etc...
	selection := ""
	if opt.LowerLetters {
		selection += LowerLetters
	}
	if opt.UpperLetters {
		selection += UpperLetters
	}
	if opt.Digits {
		selection += Digits
	}
	if opt.Symbols {
		selection += Symbols
	}
	selection += opt.ExtraSymbols
	// Make a slice based on this string
	letters := strings.Split(selection, "")
	// Remove duplicate elements
	slices.Sort(letters)
	letters = slices.Compact(letters)

	ok := false
	for i := 0; !ok; i++ {
		if pw, err = generateFromSelection(letters, opt.Length); err != nil {
			return
		}
		lower, upper, digits, symbols := true, true, true, true
		if opt.LowerLetters {
			lower = intersect(strings.Split(pw, ""), strings.Split(LowerLetters, ""))
		}
		if opt.UpperLetters {
			upper = intersect(strings.Split(pw, ""), strings.Split(UpperLetters, ""))
		}
		if opt.Digits {
			digits = intersect(strings.Split(pw, ""), strings.Split(Digits, ""))
		}
		if opt.Symbols {
			symbols = intersect(strings.Split(pw, ""), strings.Split(Symbols, ""))
		}
		ok = lower && upper && digits && symbols

		// Guard against infinite iterations, which is highly unlikely
		if i >= MaxIters {
			err = IterationError
			return
		}
	}

	return
}

// GeneratePassphrase generates a multi-word passphrase of n words.
func GeneratePassphrase(n uint) (p string, err error) {
	var words []string
	if words, err = diceware.Generate(int(n)); err != nil {
		return
	}
	p = strings.Join(words, " ")
	return
}
