package security

import (
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

func TestExtractVal(t *testing.T) {
	val1, err := extractVal[int]("v", "v=15", strconv.Atoi)
	assert.NoError(t, err)
	assert.Equal(t, 15, val1)
	val2, err := extractVal[uint32]("p", "p=42", func(s string) (uint32, error) {
		val, err := strconv.ParseUint(s, 10, 32)
		return uint32(val), err
	})
	assert.NoError(t, err)
	assert.Equal(t, uint32(42), val2)
}

func TestKeyGen(t *testing.T) {
	pw := "supersecret"
	hash1, err := KeyGen(pw, nil)
	hash2, err := KeyGen(pw, nil)
	assert.NoError(t, err)
	assert.NotEqual(t, hash1, hash2)
}

func TestPasswordHash(t *testing.T) {
	pw := "supersecret"
	hash1, err := PasswordHash(pw, nil)
	hash2, err := PasswordHash(pw, nil)
	assert.NoError(t, err)
	assert.NotEqual(t, hash1, hash2)
}

func TestVerifyHash(t *testing.T) {
	pw := "supersecret"
	hash, err := PasswordHash(pw, nil)
	assert.NoError(t, err)
	ok, err := VerifyHash(hash, pw)
	assert.NoError(t, err)
	assert.True(t, ok)
}
