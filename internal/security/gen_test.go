package security

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGeneratePassword(t *testing.T) {
	pw, err := GeneratePassword(PasswordOpt{Length: 10, LowerLetters: true, UpperLetters: true, Digits: true, ExtraSymbols: "éőüűúőüááaaa"})
	assert.NoError(t, err)
	assert.NotEmpty(t, pw)
	pw, err = GeneratePassword(PasswordOpt{Length: 16, LowerLetters: true, UpperLetters: true, Digits: true, Symbols: true})
	assert.NoError(t, err)
	assert.NotEmpty(t, pw)
	pw, err = GeneratePassword(PasswordOpt{Length: 5})
	assert.Error(t, err)
	assert.Empty(t, pw)
	pw, err = GeneratePassword(PasswordOpt{Length: 65})
	assert.Error(t, err)
	assert.Empty(t, pw)
}

func TestGeneratePassphrase(t *testing.T) {
	p, err := GeneratePassphrase(32)
	assert.NoError(t, err)
	assert.NotEmpty(t, p)
}

func TestStrength(t *testing.T) {
	p, err := GeneratePassword(PasswordOpt{Length: 12, LowerLetters: true, UpperLetters: true, Digits: true, Symbols: true})
	assert.NoError(t, err)
	assert.NotEmpty(t, p)
	s := Strength(p)
	assert.Equal(t, Strong, s)
}
