package assets

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/theme"
)

func loadIcon(name fyne.ThemeIconName) fyne.Resource {
	if data, err := Icons.ReadFile(string(name)); err != nil {
		panic(err)
	} else {
		return fyne.NewStaticResource(string(name), data)
	}
}

func loadSvg(name fyne.ThemeIconName) fyne.Resource {
	if data, err := Svgs.ReadFile(string(name)); err != nil {
		panic(err)
	} else {
		return fyne.NewStaticResource(string(name), data)
	}
}

func LogoIcon() fyne.Resource {
	return loadIcon(LogoNameIcon)
}

func AllItemsIcon() fyne.Resource {
	return theme.NewThemedResource(loadSvg(AllItemsNameIcon))
}

func CardIcon() fyne.Resource {
	return theme.NewThemedResource(loadSvg(CardNameIcon))
}

func HomeIcon() fyne.Resource {
	return theme.NewThemedResource(loadSvg(HomeNameIcon))
}

func MenuIcon() fyne.Resource {
	return theme.NewThemedResource(loadSvg(MenuNameIcon))
}

func PasswordIcon() fyne.Resource {
	return theme.NewThemedResource(loadSvg(PasswordNameIcon))
}

func SettingsIcon() fyne.Resource {
	return theme.NewThemedResource(loadSvg(SettingsNameIcon))
}

func TrashIcon() fyne.Resource {
	return theme.NewThemedResource(loadSvg(TrashNameIcon))
}
