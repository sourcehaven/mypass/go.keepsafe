package assets

import (
	"embed"
	"fyne.io/fyne/v2"
)

//go:embed ico
var Icons embed.FS

//go:embed svg
var Svgs embed.FS

type IconName string

// Icon paths
const (
	ico          = "ico/"
	svg          = "svg/"
	LogoNameIcon = ico + "mypass.png"

	// Svg icons

	AllItemsNameIcon fyne.ThemeIconName = svg + "dots-and-co-svgrepo-com.svg"
	CardNameIcon     fyne.ThemeIconName = svg + "card-emulator-pro-svgrepo-com.svg"
	HomeNameIcon     fyne.ThemeIconName = svg + "ourhome-svgrepo-com.svg"
	MenuNameIcon     fyne.ThemeIconName = svg + "menu-hamburger-svgrepo-com.svg"
	PasswordNameIcon fyne.ThemeIconName = svg + "password-svgrepo-com.svg"
	SettingsNameIcon fyne.ThemeIconName = svg + "settings-svgrepo-com.svg"
	TrashNameIcon    fyne.ThemeIconName = svg + "whatsdeleted-svgrepo-com.svg"
)
