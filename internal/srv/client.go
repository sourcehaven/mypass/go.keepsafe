package srv

import (
	"github.com/sirupsen/logrus"
	dsv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/data/v0alpha"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// NewInsecureVaultClient creates a byte vault service client,
// that you can use in your applications.
func NewInsecureVaultClient() dsv0.ByteVaultServiceClient {
	conn, err := grpc.NewClient(
		"localhost:8082",
		// Note: will need secure credentials when client-service communication is in production mode
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		logrus.Fatalf("failed to dial server with %v", err)
	}
	return dsv0.NewByteVaultServiceClient(conn)
}
