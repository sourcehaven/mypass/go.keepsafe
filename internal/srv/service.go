package srv

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"gorm.io/gorm"
	"net"
	"os"
	"os/signal"
	"syscall"
)

type Config struct {
	Host string   // specifies host - should be localhost !!! (no 0.0.0.0)
	Port uint16   // specifies the port to run the application on
	DB   *gorm.DB // specifies the db connection to use
}

type service struct {
	// unimplemented grpc service
	cfg Config
}

type gracefulService struct {
	*service // same as service, but with graceful shutdown
}

type Service interface {
	WithConfig(c ...Config) Service // lazy config loader
	Graceful() GracefulService      // make this service a graceful service
	Start()                         // start serving the application
}

type GracefulService interface {
	Service
}

func NewService(c ...Config) Service {
	if len(c) == 0 {
		return &service{}
	}
	cfg := c[0]
	// Hard coding localhost
	cfg.Host = "localhost"
	return &service{cfg: cfg}
}

func (s *service) WithConfig(c ...Config) Service {
	if len(c) == 0 {
		return &service{}
	}
	cfg := c[0]
	// Hard coding localhost
	cfg.Host = "localhost"
	return &service{cfg: cfg}
}

func (s *service) Graceful() GracefulService {
	return &gracefulService{service: s}
}

func (s *service) Start() {
	gs := grpc.NewServer()
	// Register server
	//datav0.RegisterByteVaultServiceServer(gs, s)

	host := fmt.Sprintf("%s:%d", s.cfg.Host, s.cfg.Port)
	listener, err := net.Listen("tcp", host)
	if err != nil {
		panic(err)
	}

	// Run the service.
	logrus.Infof("starting local service on %s", s.cfg.Host)
	logrus.WithFields(logrus.Fields{
		"topic": "service lifecycle",
		"error": fmt.Sprintf("%v", gs.Serve(listener)),
	}).Fatal("service stopped unexpectedly")
}

func (s *gracefulService) Start() {
	gs := grpc.NewServer()
	// Register server
	//datav0.RegisterByteVaultServiceServer(gs, s)

	host := fmt.Sprintf("%s:%d", s.cfg.Host, s.cfg.Port)
	listener, err := net.Listen("tcp", host)
	if err != nil {
		panic(err)
	}

	// Insert channel for idle channel connections.
	idle := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
		<-sigint

		// Received an interrupt signal, shutdown.
		logrus.WithFields(logrus.Fields{
			"topic": "graceful shutdown",
		}).Info("service is shutting down")
		gs.GracefulStop()

		close(idle)
	}()

	// Run the service.
	logrus.Infof("starting graceful service on %s", host)
	logrus.WithFields(logrus.Fields{
		"topic": "service lifecycle",
		"error": fmt.Sprintf("%v", gs.Serve(listener)),
	}).Fatal("service stopped unexpectedly")

	<-idle
}
