# KeepSafe project

This is a mix of service, and UI client application.
The app simultaneously runs the local client service
comprising a gRPC server that is responsible for
communication between other possible clients, while also
providing an interface to manage your vault in a hopefully
fully cross-platform manner.

Tasks:
 - Serving a gRPC service,
 - encryption,
 - decryption,
 - keeping a local representation of the database,
 - providing a simple ui,
 - be cross-platform.

## Run

Normal run
```bash
go run .
```

Debug run
```bash
go run -tags debug .
```

Emulate mobile run
```bash
go run -tags mobile .
```

Mobile packing
```bash
ANDROID_NDK_HOME=~/Android/Sdk/ndk/26.1.10909125 fyne package -os android
```

## CLI

### Initialize your vault

```bash
MYPASS_DEVEL= go run ./cmd/mypass init
```

### Read entries

```bash
MYPASS_DEVEL= go run ./cmd/mypass select --itsme=<your-profile>
```

### For more

See:

```bash
MYPASS_DEVEL= go run ./cmd/mypass --help
```

## Development dependencies

### Unix

> wl-clipboard, xsel, xclip

Run

```bash
pacman -Suy --needed wl-clipboard xsel xclip
```

or equivalent in your distribution.
If you are using wayland, it should enough to install e.g. `wl-clipboard`,
while if you are using an x11 session, you should be choosing either `xsel`, or `xclip` package.
