//go:build linux || windows || android

package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/data"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/devel"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/ui"
	"gorm.io/gorm"
	"os"
	"time"
)

var db *gorm.DB

func init() {
	logrus.SetLevel(logrus.DebugLevel)

	db = data.Connect("file::memory:")
	if err := db.AutoMigrate(
		&model.Password{},
		&model.Folder{},
		&model.Note{},
	); err != nil {
		logrus.WithFields(logrus.Fields{
			"topic": "app initialization",
			"err":   fmt.Sprintf("%v", err),
		}).Panic("application startup failed")
	}

	if _, ok := os.LookupEnv("MYPASS_DEVEL"); ok {
		devel.InitFyne()
	}
}

func main() {
	gui := ui.NewApp(ui.NewAppConfig(10 * time.Minute))

	gui.ShowAndRun()

	/*if gui.IsDesktop() {
		c := srv.Config{DB: db}
		service := run.Service(c)
		defer close(service)
	}

	gui.ShowAndRun()*/
}
