package commands

import (
	"bufio"
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v3"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/cmd/mypass/internal/app"
	serialv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/run"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/security"
	"golang.org/x/term"
	"os"
	"strconv"
	"strings"
	"syscall"
)

var (
	PasswordsDoNotMatchError = errors.New("passwords do not match")
)

func ini(ctx context.Context, command *cli.Command) (err error) {
	// Generate secure and unique token.
	// This token will be used as your master key.
	logrus.Infoln("Initializing random token.")
	token := make([]byte, run.DefaultConfig.TokenSize())
	if _, err = rand.Read(token); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	logrus.Infoln("Your random token is initialized.")

	// Read and confirm passwords
	fmt.Println("Give me a secure password (your token will be encrypted using your password):")
	pwBytes, err := term.ReadPassword(syscall.Stdin)
	if err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	fmt.Println("Confirm your password:")
	pwConfirmBytes, err := term.ReadPassword(syscall.Stdin)
	if err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	pw := string(pwBytes)
	pwConfirm := string(pwConfirmBytes)

	if pw != pwConfirm {
		fmt.Println("Your passwords do not match.")
		return PasswordsDoNotMatchError
	}

	var passwordSealedToken []byte
	if passwordSealedToken, err = security.SecureBytesPw(token, pw); err != nil {
		return
	}

	// Passphrase generation on demand
	fmt.Println("Generate passphrase in case you forget your password? " +
		"(0 - do not generate, N>0 - generate N number of words)")

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	n := 0
	if n, err = strconv.Atoi(scanner.Text()); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	var passphrase string
	if n > 0 {
		if passphrase, err = security.GeneratePassphrase(uint(n)); err != nil {
			logrus.Errorf("%v\n", err)
			return
		}
	}

	var passphraseSealedToken []byte
	if passphraseSealedToken, err = security.SecureBytesPw(token, passphrase); err != nil {
		return
	}

	logrus.Infof("Secret token: %s\n", base64.StdEncoding.EncodeToString(token))
	logrus.Infof("Passphrase: %s\n", passphrase)

	// Save credentials
	fmt.Println("Choose an ID for your secrets:")
	scanner.Scan()
	id := scanner.Text()

	var cred fyne.URI
	if cred, err = storage.Child(app.Sys.HomeDir(), fmt.Sprintf("%s.bin", id)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	write := true
	// Checking if file already exists under identity
	if ok, _ := storage.Exists(cred); ok {
		// File already exists
		logrus.Infoln("This user or identity already exists.")
		fmt.Println("Do you wish to overwrite the file? ALL DATA WILL BE LOST! (y/N)")

		scanner.Scan()
		ans := scanner.Text()
		write = len(ans) > 0 && strings.ToLower(ans)[0] == 'y'
	}

	if !write {
		logrus.Infoln("Aborting ...")
		return
	}

	var empty []byte
	if empty, err = security.SecureBytesPw(make([]byte, 0), base64.StdEncoding.EncodeToString(token)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	// Write credentials to disk (encrypted)
	cache := &serialv0.Cache{
		Id:         id,
		Password:   passwordSealedToken,
		Passphrase: passphraseSealedToken,
		Data:       empty,
	}
	if err = run.DefaultCache().Dump(cred, cache); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	logrus.Infof("Your secrets are saved under: %s\n", cred.Path())
	return
}

func BuildInit() *cli.Command {
	return &cli.Command{
		Name:   "init",
		Usage:  "Initializes your local vault",
		Action: ini,
	}
}
