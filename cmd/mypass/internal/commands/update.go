package commands

import (
	"bufio"
	"context"
	"encoding/base64"
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v3"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/cmd/mypass/internal/app"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/data"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	serialv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/run"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/security"
	datapkg "gitlab.com/sourcehaven/mypass/go.lcdbman/pkg/data"
	"golang.org/x/term"
	"os"
	"strconv"
	"strings"
	"syscall"
)

// handleEmptyStr returns a string value based on the following conditions:
//   - it will return the default value (def) if the given string (s) is empty,
//   - it will return empty if s is "-",
//   - it will return "-" if "-" is escaped via backslash "\-",
//   - otherwise it returns s.
func handleEmptyStr(s string, def string) string {
	if s == "" {
		return def
	}
	if s == "-" {
		return ""
	}
	if s == "\\-" {
		return "-"
	}
	return s
}

// handleEmptyBool returns a bool value based on the given string (s)
//   - if the string is not empty, it checks whether the input is (y(es)),
//     and returns a value based on that condition,
//   - otherwise it returns the given default (def) value.
func handleEmptyBool(s string, def bool) bool {
	if len(s) > 0 {
		return strings.ToLower(s)[0] == 'y'
	}
	return def
}

func update(ctx context.Context, command *cli.Command) (err error) {
	uid := command.String("itsme")
	logrus.Debug("User ID: ", uid)

	// Master password is needed to unlock the secret token
	fmt.Print("master password: ")
	var masterBytes []byte
	if masterBytes, err = term.ReadPassword(syscall.Stdin); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	master := string(masterBytes)
	fmt.Println()

	// Read the credential file
	var cred fyne.URI
	if cred, err = storage.Child(app.Sys.HomeDir(), fmt.Sprintf("%s.bin", uid)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	var cache serialv0.Cache
	if err = run.DefaultCache().Load(cred, &cache); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	pwSealedToken := cache.Password
	var token []byte
	if token, err = security.PlainBytesPw(pwSealedToken, master); err != nil {
		logrus.Errorf("%v\n", err)
		logrus.Infoln("Failed to decrypt your secret token. Aborting ...")
		return
	}

	scanner := bufio.NewScanner(os.Stdin)

	// Handle id param
	idFlag := command.String("id")
	var id uint64
	if id, err = strconv.ParseUint(idFlag, 10, 64); err != nil {
		// Prompt the user for a valid id
		// if invalid or missing
		fmt.Print("Update password with ID: ")
		scanner.Scan()
		var i int
		if i, err = strconv.Atoi(scanner.Text()); err != nil {
			logrus.Errorf("%v\n", err)
			return
		}
		id = uint64(i)
	}

	// Load the database first
	db := data.Connect("file::memory:")
	if err = db.AutoMigrate(
		&model.Password{},
		&model.Folder{},
		&model.Note{},
	); err != nil {
		panic(err)
	}
	seri := data.NewVaultSerializer(data.NewVaultStore(db))

	// Decrypting data
	var plainData []byte
	if plainData, err = security.PlainBytesPw(cache.Data, base64.StdEncoding.EncodeToString(token)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	// Load the decrypted data
	if err = seri.Load(plainData); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	pwDao := datapkg.NewSimpleDao[uint, model.Password](datapkg.Config{DB: db})
	var pwItem model.Password
	if pwItem, err = pwDao.FindById(uint(id)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	show := command.Bool("plain")
	key := "******"
	if show {
		key = pwItem.Password
	}
	fav := "no"
	if pwItem.Favourite {
		fav = "yes"
	}

	fmt.Println("-- ------- Selected Password ------- --")
	fmt.Printf("ID:        %25d\n", pwItem.ID)
	fmt.Printf("Name:      %25s\n", pwItem.Name)
	fmt.Printf("Username:  %25s\n", pwItem.Username)
	fmt.Printf("Password:  %25s\n", key)
	fmt.Printf("Site:      %25s\n", pwItem.Site)
	fmt.Printf("Notes:     %25s\n", pwItem.Note)
	fmt.Printf("Favourite: %25s\n", fav)
	fmt.Println(" . . .")

	// Read data interactively
	fmt.Printf("identifier (optional/unchanged) - %s: ", pwItem.Name)
	scanner.Scan()
	name := scanner.Text()
	name = handleEmptyStr(name, pwItem.Name)

	fmt.Printf("username (optional/unchanged) - %s: ", pwItem.Username)
	scanner.Scan()
	username := scanner.Text()
	username = handleEmptyStr(username, pwItem.Username)

	fmt.Printf("password (optional/unchanged) - %s: ", key)
	var pwBytes []byte
	if pwBytes, err = term.ReadPassword(syscall.Stdin); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	password := string(pwBytes)
	fmt.Println() // term.ReadPassword does not break lines
	password = handleEmptyStr(password, pwItem.Password)

	fmt.Printf("site (optional/unchanged) - %s: ", pwItem.Site)
	scanner.Scan()
	site := scanner.Text()
	site = handleEmptyStr(site, pwItem.Site)

	fmt.Printf("notes (optional/unchanged) - %s: ", pwItem.Note)
	scanner.Scan()
	note := scanner.Text()
	note = handleEmptyStr(note, pwItem.Note)

	fmt.Printf("favourite (y/N/unchanged) - %s: ", fav)
	scanner.Scan()
	fav = scanner.Text()
	favourite := handleEmptyBool(fav, pwItem.Favourite)

	pw := &model.Password{
		Name:      name,
		Username:  username,
		Password:  password,
		Site:      site,
		Note:      note,
		Favourite: favourite,
	}

	var ok bool
	if ok, err = pwDao.K4L().SaveById(uint(id), pw); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	if !ok {
		logrus.Errorf("no record was updated with id %d", id)
		return
	}

	// CacheDump records
	var dump []byte
	if dump, err = seri.Dump(); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	// Encrypting data
	var secureData []byte
	if secureData, err = security.SecureBytesPw(dump, base64.StdEncoding.EncodeToString(token)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	cache.Data = secureData

	// Save the actual vault
	if err = run.DefaultCache().Dump(cred, &cache); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	logrus.Debugln("Password is updated.", pw)
	return
}

func BuildUpdate() *cli.Command {
	return &cli.Command{
		Name:   "update",
		Usage:  "Updates a given password entry",
		Action: update,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "itsme",
				Required: true,
				Aliases:  []string{"I"},
				Usage:    "user identifier of the vault",
			},
			&cli.StringFlag{
				Name:     "id",
				Required: false,
				Aliases:  []string{"i"},
				Usage:    "unique identifier of desired vault entry",
			},
			&cli.BoolFlag{
				Name:     "plain",
				Required: false,
				Aliases:  []string{"p"},
				Usage:    "show plain text passwords instead of stars",
			},
			&cli.BoolFlag{
				Name:     "force",
				Required: false,
				Aliases:  []string{"f"},
				Usage:    "force delete will remove soft deleted entries",
			},
		},
	}
}
