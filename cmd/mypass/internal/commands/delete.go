package commands

import (
	"bufio"
	"context"
	"encoding/base64"
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v3"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/cmd/mypass/internal/app"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/data"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	serialv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/run"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/security"
	datapkg "gitlab.com/sourcehaven/mypass/go.lcdbman/pkg/data"
	"golang.org/x/term"
	"os"
	"strconv"
	"syscall"
)

func delete(ctx context.Context, command *cli.Command) (err error) {
	uid := command.String("itsme")
	logrus.Debug("User ID: ", uid)

	// Master password is needed to unlock the secret token
	fmt.Print("master password: ")
	var masterBytes []byte
	if masterBytes, err = term.ReadPassword(syscall.Stdin); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	master := string(masterBytes)
	fmt.Println()

	// Read the credential file
	var cred fyne.URI
	if cred, err = storage.Child(app.Sys.HomeDir(), fmt.Sprintf("%s.bin", uid)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	var cache serialv0.Cache
	if err = run.DefaultCache().Load(cred, &cache); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	pwSealedToken := cache.Password
	var token []byte
	if token, err = security.PlainBytesPw(pwSealedToken, master); err != nil {
		logrus.Errorf("%v\n", err)
		logrus.Infoln("Failed to decrypt your secret token. Aborting ...")
		return
	}

	scanner := bufio.NewScanner(os.Stdin)

	idFlag := command.String("id")
	var id uint64
	if id, err = strconv.ParseUint(idFlag, 10, 64); err != nil {
		// Prompt the user for a valid id
		// if invalid or missing
		fmt.Print("Delete password with ID: ")
		scanner.Scan()
		var i int
		if i, err = strconv.Atoi(scanner.Text()); err != nil {
			logrus.Errorf("%v\n", err)
			return
		}
		id = uint64(i)
	}

	// Load the database first
	db := data.Connect("file::memory:")
	if err = db.AutoMigrate(
		&model.Password{},
		&model.Folder{},
		&model.Note{},
	); err != nil {
		panic(err)
	}
	seri := data.NewVaultSerializer(data.NewVaultStore(db))

	// Decrypting data
	var plainData []byte
	if plainData, err = security.PlainBytesPw(cache.Data, base64.StdEncoding.EncodeToString(token)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	// Load the decrypted data
	if err = seri.Load(plainData); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	// Delete record from db
	pwDao := datapkg.NewSimpleDao[uint, model.Password](datapkg.Config{DB: db})

	force := command.Bool("force")
	var ok bool
	if force {
		if ok, err = pwDao.Me2().DeleteById(uint(id)); err != nil {
			logrus.Errorf("%v\n", err)
			return
		}
	} else {
		if ok, err = pwDao.K4L().DeleteById(uint(id)); err != nil {
			logrus.Errorf("%v\n", err)
			return
		}
	}
	if !ok {
		logrus.Errorf("Failed to delete password with id %d\n", id)
		return
	}

	// CacheDump records
	var dump []byte
	if dump, err = seri.Dump(); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	// Encrypting data
	var secureData []byte
	if secureData, err = security.SecureBytesPw(dump, base64.StdEncoding.EncodeToString(token)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	cache.Data = secureData

	// Save the actual vault
	if err = run.DefaultCache().Dump(cred, &cache); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	logrus.Debugln("Password is deleted.")
	return
}

func BuildDelete() *cli.Command {
	return &cli.Command{
		Name:    "delete",
		Usage:   "Deletes a given password from the vault",
		Aliases: []string{"rm"},
		Action:  delete,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "itsme",
				Required: true,
				Aliases:  []string{"I"},
				Usage:    "user identifier of the vault",
			},
			&cli.StringFlag{
				Name:     "id",
				Required: false,
				Aliases:  []string{"i"},
				Usage:    "unique identifier of desired vault entry",
			},
			&cli.BoolFlag{
				Name:     "force",
				Required: false,
				Aliases:  []string{"f"},
				Usage:    "force delete will remove soft deleted entries",
			},
		},
	}
}
