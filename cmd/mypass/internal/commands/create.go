package commands

import (
	"bufio"
	"context"
	"encoding/base64"
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v3"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/cmd/mypass/internal/app"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/data"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	serialv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/run"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/security"
	datapkg "gitlab.com/sourcehaven/mypass/go.lcdbman/pkg/data"
	"golang.org/x/term"
	"os"
	"strings"
	"syscall"
)

func create(ctx context.Context, command *cli.Command) (err error) {
	uid := command.String("itsme")
	logrus.Debug("User ID: ", uid)

	// Master password is needed to unlock the secret token
	fmt.Print("master password: ")
	var masterBytes []byte
	if masterBytes, err = term.ReadPassword(syscall.Stdin); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	master := string(masterBytes)
	fmt.Println()

	// Read the credential file
	var cred fyne.URI
	if cred, err = storage.Child(app.Sys.HomeDir(), fmt.Sprintf("%s.bin", uid)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	var cache serialv0.Cache
	if err = run.DefaultCache().Load(cred, &cache); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	pwSealedToken := cache.Password
	var token []byte
	if token, err = security.PlainBytesPw(pwSealedToken, master); err != nil {
		logrus.Errorf("%v\n", err)
		logrus.Infoln("Failed to decrypt your secret token. Aborting ...")
		return
	}

	scanner := bufio.NewScanner(os.Stdin)

	// Read data interactively
	fmt.Print("identifier (optional): ")
	scanner.Scan()
	name := scanner.Text()
	fmt.Print("username (optional): ")
	scanner.Scan()
	username := scanner.Text()
	fmt.Print("password (optional): ")
	var pwBytes []byte
	if pwBytes, err = term.ReadPassword(syscall.Stdin); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	password := string(pwBytes)
	fmt.Println() // term.ReadPassword does not break lines
	fmt.Print("site (optional): ")
	scanner.Scan()
	site := scanner.Text()
	fmt.Print("notes (optional): ")
	scanner.Scan()
	note := scanner.Text()
	fmt.Print("favourite (y/N): ")
	scanner.Scan()
	fav := scanner.Text()
	favourite := len(fav) > 0 && strings.ToLower(fav)[0] == 'y'

	pw := &model.Password{
		Name:      name,
		Username:  username,
		Password:  password,
		Site:      site,
		Note:      note,
		Favourite: favourite,
	}

	// Load the database first
	db := data.Connect("file::memory:")
	if err = db.AutoMigrate(
		&model.Password{},
		&model.Folder{},
		&model.Note{},
	); err != nil {
		panic(err)
	}
	seri := data.NewVaultSerializer(data.NewVaultStore(db))

	// Decrypting data
	var plainData []byte
	if plainData, err = security.PlainBytesPw(cache.Data, base64.StdEncoding.EncodeToString(token)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	// Load the decrypted data
	if err = seri.Load(plainData); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	// Add record to db
	pwDao := datapkg.NewSimpleDao[uint, model.Password](datapkg.Config{DB: db})

	if err = pwDao.Create(pw); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	// CacheDump records
	var dump []byte
	if dump, err = seri.Dump(); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	// Encrypting data
	var secureData []byte
	if secureData, err = security.SecureBytesPw(dump, base64.StdEncoding.EncodeToString(token)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	cache.Data = secureData

	// Save the actual vault
	if err = run.DefaultCache().Dump(cred, &cache); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	logrus.Debugln("Password is saved.", pw)
	return
}

func BuildCreate() *cli.Command {
	return &cli.Command{
		Name:    "create",
		Usage:   "Adds a new password to vault",
		Action:  create,
		Aliases: []string{"add"},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "itsme",
				Required: true,
				Aliases:  []string{"I"},
				Usage:    "user identifier of the vault",
			},
		},
	}
}
