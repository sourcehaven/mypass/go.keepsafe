package commands

import (
	"context"
	"encoding/base64"
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v3"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/cmd/mypass/internal/app"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/clipboard"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/data"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	serialv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/run"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/security"
	datapkg "gitlab.com/sourcehaven/mypass/go.lcdbman/pkg/data"
	"golang.org/x/term"
	"strconv"
	"syscall"
)

func selectAll(ctx context.Context, command *cli.Command) (err error) {
	uid := command.String("itsme")
	logrus.Debug("User ID: ", uid)

	// Master password is needed to unlock the secret token
	fmt.Print("master password: ")
	var masterBytes []byte
	if masterBytes, err = term.ReadPassword(syscall.Stdin); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	master := string(masterBytes)
	fmt.Println()

	// Read the credential file
	var cred fyne.URI
	if cred, err = storage.Child(app.Sys.HomeDir(), fmt.Sprintf("%s.bin", uid)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	var cache serialv0.Cache
	if err = run.DefaultCache().Load(cred, &cache); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	pwSealedToken := cache.Password
	var token []byte
	if token, err = security.PlainBytesPw(pwSealedToken, master); err != nil {
		logrus.Errorf("%v\n", err)
		logrus.Infoln("Failed to decrypt your secret token. Aborting ...")
		return
	}

	// Load the database first
	db := data.Connect("file::memory:")
	if err = db.AutoMigrate(
		&model.Password{},
		&model.Folder{},
		&model.Note{},
	); err != nil {
		panic(err)
	}
	seri := data.NewVaultSerializer(data.NewVaultStore(db))

	// Decrypting data
	var plainData []byte
	if plainData, err = security.PlainBytesPw(cache.Data, base64.StdEncoding.EncodeToString(token)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	// Load the decrypted data
	if err = seri.Load(plainData); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	// List all records from db
	pwDao := datapkg.NewSimpleDao[uint, model.Password](datapkg.Config{DB: db})

	show := command.Bool("plain")
	deleted := command.Bool("deleted")

	var pw []model.Password
	if deleted {
		if pw, err = pwDao.Me2().FindAll(); err != nil {
			logrus.Errorf("%v\n", err)
			return
		}
	} else {
		if pw, err = pwDao.FindAll(); err != nil {
			logrus.Errorf("%v\n", err)
			return
		}
	}

	for i, pwItem := range pw {
		key := "******"
		if show {
			key = pwItem.Password
		}
		fav := "no"
		if pwItem.Favourite {
			fav = "yes"
		}
		del := "no"
		if pwItem.DeletedAt.Valid {
			del = "yes"
		}

		fmt.Printf("-- ------------ %04d ------------ --\n", i+1)
		fmt.Printf("ID:        %25d\n", pwItem.ID)
		fmt.Printf("Name:      %25s\n", pwItem.Name)
		fmt.Printf("Username:  %25s\n", pwItem.Username)
		fmt.Printf("Password:  %25s\n", key)
		fmt.Printf("Site:      %25s\n", pwItem.Site)
		fmt.Printf("Notes:     %25s\n", pwItem.Note)
		fmt.Printf("Favourite: %25s\n", fav)
		if deleted { // only print deleted attribute if flag is given
			fmt.Printf("Deleted:   %25s\n", del)
		}
	}

	return
}

func copyPw(ctx context.Context, command *cli.Command) (err error) {
	uid := command.String("itsme")
	logrus.Debug("User ID: ", uid)

	// Master password is needed to unlock the secret token
	fmt.Print("master password: ")
	var masterBytes []byte
	if masterBytes, err = term.ReadPassword(syscall.Stdin); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	master := string(masterBytes)
	fmt.Println()

	// Read the credential file
	var cred fyne.URI
	if cred, err = storage.Child(app.Sys.HomeDir(), fmt.Sprintf("%s.bin", uid)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	var cache serialv0.Cache
	if err = run.DefaultCache().Load(cred, &cache); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	pwSealedToken := cache.Password
	var token []byte
	if token, err = security.PlainBytesPw(pwSealedToken, master); err != nil {
		logrus.Errorf("%v\n", err)
		logrus.Infoln("Failed to decrypt your secret token. Aborting ...")
		return
	}

	// Load the database first
	db := data.Connect("file::memory:")
	if err = db.AutoMigrate(
		&model.Password{},
		&model.Folder{},
		&model.Note{},
	); err != nil {
		panic(err)
	}
	seri := data.NewVaultSerializer(data.NewVaultStore(db))

	// Decrypting data
	var plainData []byte
	if plainData, err = security.PlainBytesPw(cache.Data, base64.StdEncoding.EncodeToString(token)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	// Load the decrypted data
	if err = seri.Load(plainData); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	// List all records from db
	pwDao := datapkg.NewSimpleDao[uint, model.Password](datapkg.Config{DB: db})

	idFlag := command.String("id")
	var id uint64
	if id, err = strconv.ParseUint(idFlag, 10, 64); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	var pw model.Password
	if pw, err = pwDao.FindById(uint(id)); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}

	password := pw.Password
	c := clipboard.System()
	if err = c.Push(password); err != nil {
		logrus.Errorf("%v\n", err)
		return
	}
	logrus.Infoln("Password text copied to clipboard.")
	return
}

func BuildRead() *cli.Command {
	return &cli.Command{
		Name:    "read",
		Usage:   "Lists vault entries",
		Aliases: []string{"select"},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "itsme",
				Required: true,
				Aliases:  []string{"I"},
				Usage:    "user identifier of the vault",
			},
		},
		Commands: []*cli.Command{
			{
				Name:   "all",
				Usage:  "Lists all entries",
				Action: selectAll,
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:     "plain",
						Required: false,
						Aliases:  []string{"p"},
						Usage:    "show plain text passwords instead of stars",
					},
					&cli.BoolFlag{
						Name:     "deleted",
						Required: false,
						Aliases:  []string{"d"},
						Usage:    "show only deleted passwords",
					},
				},
			},
			{
				Name:   "copy",
				Usage:  "Copies password to clipboard",
				Action: copyPw,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "id",
						Required: true,
						Aliases:  []string{"i"},
						Usage:    "unique identifier of desired vault entry",
					},
				},
			},
		},
	}
}
