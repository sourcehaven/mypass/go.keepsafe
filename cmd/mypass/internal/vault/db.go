package vault

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/data"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/model"
	serialv0 "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/proto/serial/v0alpha"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/run"
	"gorm.io/gorm"
)

type store struct {
	DB       *gorm.DB
	id       string
	password string
}

var Local *store

func init() {
	db := data.Connect("file::memory:")
	if err := db.AutoMigrate(
		&model.Password{},
		&model.Folder{},
		&model.Note{},
	); err != nil {
		panic(err)
	}

	Local = &store{DB: db}
}

func (s *store) getHome() (home fyne.URI, err error) {
	if home, err = storage.Child(fyne.CurrentApp().Storage().RootURI(), fmt.Sprintf("%s.bin", s.id)); err != nil {
		return
	}
	return
}

func (s *store) getCache() (cache serialv0.Cache, err error) {
	var home fyne.URI
	if home, err = s.getHome(); err != nil {
		return
	}
	if err = run.DefaultCache().Load(home, &cache); err != nil {
		return
	}
	return
}

// Start will initialize this store with your username and master password.
func (s *store) Start(id string, password string) {
	s.id = id
	s.password = password
}

// Sealed returns the password encrypted token from the cache file.
func (s *store) Sealed() {

}

// Save will save the database to the `data` part of the cache.
func (s *store) Save() (err error) {
	var cache serialv0.Cache
	var home fyne.URI
	if home, err = s.getHome(); err != nil {
		return
	}
	if err = run.DefaultCache().Load(home, &cache); err != nil {
		return
	}

	seri := data.NewVaultSerializer(data.NewVaultStore(s.DB))
	var dump []byte
	if dump, err = seri.Dump(); err != nil {
		return err
	}

	// TODO: these should be encrypted first.
	cache.Data = dump
	if err = run.DefaultCache().Dump(home, &cache); err != nil {
		return
	}
	return
}

// Load will load the `data` part of the cache,
// parsing it to the in-memory database.
func (s *store) Load() (err error) {
	var home fyne.URI
	var cache serialv0.Cache
	if home, err = s.getHome(); err != nil {
		return
	}
	if err = run.DefaultCache().Load(home, &cache); err != nil {
		return
	}

	seri := data.NewVaultSerializer(data.NewVaultStore(s.DB))
	dump := cache.Data
	// TODO: data should be decrypted before loading
	if err = seri.Load(dump); err != nil {
		return
	}
	return
}
