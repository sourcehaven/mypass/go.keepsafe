package app

import (
	"encoding/binary"
	"errors"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/storage"
	storagex "gitlab.com/sourcehaven/mypass/go.keepsafe/internal/x/storage"
	"time"
)

var (
	MalformedSessionFileErr = errors.New("malformed session file")
)

type session struct {
	home string
}

type PasswordSession interface {
	// Save helps to store a session-like file,
	// to keep track of the time a master password was given.
	// Using this, you may only be prompted for your master
	// password after a specific time.
	Save(user string, timeout time.Duration) (err error)
	// IsActive reports whether a master password session is
	// still active on a user, or not.
	IsActive(user string) (ok bool, err error)
}

func NewPasswordSession(home string) (s PasswordSession, err error) {
	if err = storagex.MakeDirs(home); err != nil {
		return
	}
	s = &session{home: home}
	return
}

func (s *session) Save(user string, timeout time.Duration) (err error) {
	var f fyne.URI
	home := storage.NewFileURI(s.home)
	now := time.Now().UTC()
	// TODO: username should be sanitized -> disallowed chars should be removed, or replaced
	if f, err = storage.Child(home, user); err != nil {
		return
	}
	var writer fyne.URIWriteCloser
	if writer, err = storage.Writer(f); err != nil {
		return
	}
	defer func(writer fyne.URIWriteCloser) {
		if err2 := writer.Close(); err2 != nil {
			err = err2
		}
	}(writer)

	buf := make([]byte, 8)
	ts := now.Add(timeout).Unix()
	binary.PutVarint(buf, ts)
	if _, err = writer.Write(buf); err != nil {
		return
	}

	return
}

func (s *session) IsActive(user string) (ok bool, err error) {
	var f fyne.URI
	home := storage.NewFileURI(s.home)
	if f, err = storage.Child(home, user); err != nil {
		return
	}
	var res fyne.Resource
	if res, err = storage.LoadResourceFromURI(f); err != nil {
		return
	}
	ts, n := binary.Varint(res.Content())
	if n <= 0 {
		return false, MalformedSessionFileErr
	}
	epoch := time.Unix(ts, 0).UTC()
	if time.Now().UTC().Before(epoch) {
		ok = true
	} else {
		ok = false
	}
	return
}
