package app

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/storage"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/devel"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/internal/run"
	"os"
	"os/signal"
	"syscall"
)

type Runtime interface {
	App() fyne.App
	HomeDir() fyne.URI
}

type runtime struct {
	app fyne.App
}

func (r *runtime) App() fyne.App {
	return r.app
}

func (r *runtime) HomeDir() fyne.URI {
	return storage.NewFileURI(run.DefaultConfig.AppDir().Path())
}

var Sys Runtime

func init() {
	// Initialize runtime components
	if _, ok := os.LookupEnv("MYPASS_DEVEL"); ok {
		devel.InitFyne()
	}
	myApp := app.New()
	Sys = &runtime{
		app: myApp,
	}
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	go func() {
		<-sig
		Sys.App().Quit()
	}()
}
