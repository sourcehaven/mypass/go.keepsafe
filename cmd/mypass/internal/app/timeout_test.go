package app

import (
	"github.com/stretchr/testify/assert"
	"os"
	"path"
	"testing"
	"time"
)

func TestSession_Save(t *testing.T) {
	tmp := os.TempDir()
	s, err := NewPasswordSession(path.Join(tmp, "mypass", "session"))
	assert.NoError(t, err)
	assert.NoError(t, s.Save("test", time.Second))
}

func TestIsPasswordSessionActive(t *testing.T) {
	tmp := os.TempDir()
	s, err := NewPasswordSession(path.Join(tmp, "mypass", "session"))
	assert.NoError(t, err)
	assert.NoError(t, s.Save("test", time.Second*30))
	ok, err := s.IsActive("test")
	assert.NoError(t, err)
	assert.True(t, ok)
}
