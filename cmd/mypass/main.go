//go:build linux || windows

package main

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v3"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/cmd/mypass/internal/app"
	"gitlab.com/sourcehaven/mypass/go.keepsafe/cmd/mypass/internal/commands"
	"os"
)

func init() {
	// TODO: set logrus level from config
	logrus.SetLevel(logrus.DebugLevel)
}

func main() {
	cmd := &cli.Command{
		Name:        "mypass",
		Description: "MyPass - a simplistic, multiplatform, and extendable local password manager in go.",
		Commands: []*cli.Command{
			commands.BuildInit(),
			commands.BuildCreate(), // C (create)
			commands.BuildRead(),   // R (read)
			commands.BuildUpdate(), // U (update)
			commands.BuildDelete(), // D (delete)
		},
	}
	go func() {
		if err := cmd.Run(context.Background(), os.Args); err != nil {
			logrus.Fatalf("%v\n", err)
		}
		app.Sys.App().Quit()
	}()

	app.Sys.App().Run()
}
